require 'prawn'
require 'papers/helpers/file_helpers'

module Papers
  module PrawnHelpers

    include Papers::FileHelpers
    include Prawn::Measurements

    def create_document
      doc = Prawn::Document.new page_size: [mm2pt(210), mm2pt(297)], margin: 0
      set_font doc, 'gothic.ttf'
      doc.line_width = 0.1
      doc
    end

    def create_document_for_a4
      doc = Prawn::Document.new page_size: 'A4', margin: 0
      set_font doc, 'gothic.ttf'
      doc.line_width = 0.1
      doc
    end

    def set_font(doc, font)
      doc.font font_path(font)
    end

    def set_font_family(doc, font_family)
      doc.font_families.update(font_family)
    end

    def bound(doc, image, left, bottom, width, height)
      doc.bounding_box [left, height + bottom], width: width, height: height do
        fit_image doc, image, width, height
      end
    end

    def fit_image(doc, temp, width, height)
      doc.image temp.path, fit: [width, height], position: :center, vposition: :center
    end

    def gothic_font_family
      {
        'Gothic' => {
          bold: font_path('gothic-bold.ttf'),
          normal: font_path('gothic.ttf')
        }
      }
    end

  end
end
