require 'prawn'

module Papers
  module FileHelpers

    def pdf_to_temp(doc, basename)
      temp = Tempfile.new([basename, '.pdf'])
      doc.render_file(temp.path)
      temp
    end

    def image_to_temp(image, basename)
      temp = Tempfile.new(basename)
      image.write(temp.path)
      temp
    end

    def xlsx_to_temp(workbook, basename)
      temp = Tempfile.new([basename, '.xlsx'])
      workbook.write(temp)
      temp
    end

    def write_to_temp(content, basename = 'tempname')
      temp = Tempfile.new(basename)
      file = File.new(temp.path, 'wb')
      file.write(content)
      file.close
      temp
    end

    def image_path(name, project_specific = false)
      asset_path('images', name, project_specific)
    end

    def font_path(name, project_specific = false)
      asset_path('fonts', name, project_specific)
    end

    def template_path(name, project_specific = false)
      asset_path('templates', name, project_specific)
    end

    private

    def asset_path(type, name, project_specific)
      base_path = [Papers.root, 'lib', 'assets', type]
      base_path << Papers.configuration.project_name.to_s if project_specific
      File.join(base_path, name)
    end

  end
end
