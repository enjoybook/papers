require 'axlsx'

module Papers
  module AxlsxHelpers

    def create_xlsx_file(name)
      axlsx = Axlsx::Package.new
      workbook = axlsx.workbook
      sheet = workbook.add_worksheet
      yield sheet
      save_to_xlsx_file axlsx, name
    end

    def save_to_xlsx_file(axlsx, name)
      temp = Tempfile.new [name, '.xlsx']
      axlsx.serialize temp
      temp
    end

    def bordered_cell(sheet, row, text, type=nil)
      border_styles = get_border_styles
      styles = sheet.styles.add_style(**border_styles)
      row.add_cell text, style: styles, type: type
    end

    def bordered_bold_cell(sheet, row, text)
      border_styles = get_border_styles
      border_styles[:b] = true
      styles = sheet.styles.add_style(**border_styles)
      row.add_cell text, style: styles
    end

    def bordered_cell_with_small_font(sheet, row, text, bold=false)
      border_styles = get_border_styles
      border_styles[:sz] = 8
      border_styles[:b] = true if bold
      styles = sheet.styles.add_style(**border_styles)
      row.add_cell text, style: styles
    end

    def simple_styles(sheet)
      sheet.styles.add_style(
        sz: 8,
        alignment: { horizontal: :left, wrap_text: true }
      )
    end

    def get_border_styles
      {
        sz: 10,
        alignment: { wrap_text: true },
        border: { style: :thin, color: '00000000' }
      }
    end

    def set_width(sheet, table_header, data)
      widths = get_widths table_header, data
      sheet.column_widths *widths
    end

    def set_margins(sheet, top: 0.2, right: 0.2, bottom: 0.2, left: 0.2)
      sheet.page_margins.set top: top, right: right, bottom: bottom, left: left
    end

    def build_styles(sheet, font_size, alignment=nil, bold=false, border=nil)
      sheet.styles.add_style(
        sz: font_size,
        b: bold,
        alignment: alignment,
        border: border
      )
    end

    def build_border_styles(sheet, font_size=10, alignment=nil, bold=false)
      build_styles sheet, font_size, alignment, bold, { style: :thin, color: "000000" }
    end

    private

    def get_widths(table_header, data)
      keys = data.first.keys
      keys.each_with_index.map do |key, index|
        table_header_max = calc_column_width table_header[index]

        table_body_max = data.map do |d|
          str = d[key].to_s

          if str.match(/\n/)
            str = str.split("\n").max
          end

          calc_column_width str
        end.max

        [table_body_max, table_header_max].max
      end
    end

    def calc_column_width(str)
      # Alxsx sets column width in chars measure (only), meanwhile excel sets the ones
      # in inches, cms and so on. There's no way to set column width in these units and
      # there's a problem... axlsx autowidth doesn't work correctly.
      # If we set width at 30 chars, excel will elongate the column, and it will be
      # more than 30 chars by width.

      # So... if we set width at 74 chars it could contain:
      # * 82 (digits)
      # * 82 (chars)
      # * 64 (А-Я, A-Z)
      # * 41 (№)
      # * 145 (spaces; ','; '.')
      # * 193 ('i' and 'l')

      # I picked up some values to adjust column width.
      # Dots, commas and minus-hyphen are the smallest chars of all.
      # IMPORTANT! If font (or font-size) will be changed calculations will be changed too as well.
      digits_ws = chars_ws = 0.9024390243902439 # 74 / 82
      uppercase_ws = 1.15625 # 74 / 64
      number_sign_ws = 1.8048780487804879 # 74 / 41
      commas_ws = 0.5103448275862069 # 74 / 145
      il_ws = 0.38341968911917096 # 74 / 193

      digits     = str.scan(/[0-9]/).size    * digits_ws
      commas     = str.scan(/[,.\-\s]/).size * commas_ws
      chars      = str.scan(/[а-яa-z]/).size * chars_ws
      uppercases = str.scan(/[А-ЯA-Z]/).size * uppercase_ws
      numbers    = str.scan(/№/).size        * number_sign_ws

      li_size = str.scan(/[il]/).size
      li_cur = (li_size * chars_ws) - (li_size * il_ws)

      (commas + digits + chars + uppercases + numbers - li_cur).ceil + 0.3
    end

  end
end
