require 'rqrcode'

module Papers
  module QrHelpers

    def qr_code_image(link, color = 'black', size = 3, level = :m)
      qr = RQRCode::QRCode.new(link, size: size, level: level).as_png color: color
      file = Tempfile.new(['qrcode', '.png'])
      qr.save file.path
      file
    end

  end
end
