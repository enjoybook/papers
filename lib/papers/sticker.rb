require 'papers/paper_factory'
require 'papers/stickers/enjoybook'
require 'papers/stickers/mynamebook'

module Papers
  class Sticker

    class << self
      def new
        Papers::PaperFactory.build('Stickers')
      end
    end

  end
end
