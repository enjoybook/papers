require 'papers/helpers/axlsx_helpers'

module Papers
  class CourierRegister

    include Papers::AxlsxHelpers

    def generate_file(entities, basename = 'courier register')
      create_xlsx_file(basename) do |sheet|
        add_title sheet
        add_header sheet
        add_content sheet, prepare_data(entities)
        add_info sheet, entities.size
        add_settings sheet
      end
    end

    private

    MYNAMEBOOK_ADDRESS = 'Переселенцев 1/1'.freeze
    ENJOYBOOK_ADDRESS  = 'Туркестанская 61'.freeze

    def prepare_data(entities)
      entities.each_with_index.map do |entity, index|
        entity_id = "#{entity.prefix}#{entity.id}"

        {
          index: index + 1,
          position: 1,
          type: entity.product_name,
          address: entity.address.source,
          city: '',
          street: '',
          building: '',
          id: entity_id,
          username: entity.username,
          phone: entity.phone,
          provoice_number: '',
          value_for_money: '',
          barcode: '',
          delivery_date: '',
          delivery_time: '',
          email: '',
          amount: ''
        }
      end
    end

    def add_title(sheet)
      styles = simple_styles sheet
      sheet.merge_cells 'C1:F1'
      sheet.merge_cells 'C2:D2'
      sheet.merge_cells 'B3:C3'
      sheet.merge_cells 'B4:C4'
      sheet.merge_cells 'B5:C5'
      sheet.merge_cells 'D5:E5'
      sheet.merge_cells 'B6:C6'
      sheet.merge_cells 'B7:C7'
      sheet.merge_cells 'B8:C8'
      sheet.merge_cells 'G5:H5'
      sheet.merge_cells 'G6:H6'
      sheet.merge_cells 'G7:H7'
      sheet.merge_cells 'E8:F8'

      sheet.add_row [nil, nil, "Список почтовых заказов (наложенным платежом) от #{DateTime.now.strftime '%e.%m.%y'}"], style: styles, height: 35
      sheet.add_row [nil, nil, 'ЗАКАЗ-НАРЯД №__'], style: styles
      sheet.add_row [nil, 'Дата заказа:'], style: styles
      sheet.add_row [nil, 'Договор №'], style: styles
      sheet.add_row [nil, 'Исполнитель:', nil, 'Пони-Экспресс', nil, 'Заказчик:', 'ИП Арестов А.А.'], style: styles
      sheet.add_row [nil, 'Адрес:', nil, nil, nil, 'Адрес:', address], style: styles
      sheet.add_row [nil, 'Телефон:', nil, nil, nil, 'Телефон:', Papers.configuration.phone], style: styles
      sheet.add_row [nil, 'Проект:', nil, nil, "#{Papers.configuration.project_name}.ru"], style: styles
    end

    def add_header(sheet)
      styles = build_border_styles sheet, 8, { wrap_text: true }, true

      headers = [
        '№ п/п',
        'к-во мест',
        'описание одного места',
        'область получателя',
        'город получателя',
        'улица',
        'дом, строение, корпус, квартира',
        'номер заказа',
        'получатель',
        'телефон',
        '№ присвойки',
        'сумма объявленной ценности, руб.',
        'штрих код',
        'дата доставки',
        'время доставки',
        'e-mail',
        'сумма к оплате получателем'
      ]

      sheet.add_row headers, style: styles, height: 75
    end

    def add_content(sheet, data)
      styles = build_border_styles sheet, 8, { wrap_text: true }

      data.each do |d|
        values = [
          d[:index],
          d[:position],
          d[:type],
          d[:address],
          d[:city],
          d[:street],
          d[:building],
          d[:id],
          d[:username],
          d[:phone],
          d[:provoice_number],
          d[:value_for_money],
          d[:barcode],
          d[:delivery_date],
          d[:delivery_time],
          d[:email],
          d[:amount]
        ]

        sheet.add_row values, style: styles, height: 45
      end
    end

    def add_info(sheet, entities)
      styles = simple_styles sheet
      sheet.add_row [nil]
      sheet.add_row [nil]
      sheet.add_row ['СДАЛ:', nil, nil, nil, 'КОЛИЧЕСТВО МЕСТ:', nil, entities.size, 'ПРИНЯЛ:'], style: styles
      sheet.merge_cells sheet.rows.last.cells[(0..1)]
      sheet.merge_cells sheet.rows.last.cells[(4..5)]
    end

    def add_settings(sheet)
      set_margins sheet
      sheet.column_widths 3, 4, 8, 8.5, 8.5, 7, 9, 5, 7, 9, 5, 6, 5, 5, 5, 5, 6
    end

    def project_name
      Papers.configure(&:project_name)
    end

    def address
      project_name == :mynamebook ? MYNAMEBOOK_ADDRESS : ENJOYBOOK_ADDRESS
    end

  end
end
