require 'papers/paper_factory'
require 'papers/pony_courier_registers/enjoybook'
require 'papers/pony_courier_registers/mynamebook'

module Papers
  class PonyCourierRegister

    class << self
      def new
        Papers::PaperFactory.build('PonyCourierRegisters')
      end
    end

  end
end
