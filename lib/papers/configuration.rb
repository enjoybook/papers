module Papers
  class Configuration

    attr_accessor :project_name, :phone, :email, :postcode

  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configuration=(config)
    @configuration = config
  end

  def self.configure
    yield configuration
  end
end
