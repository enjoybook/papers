require 'papers/stickers/base'
require 'papers/barcodes/track_barcode'

module Papers
  module Stickers
    class Enjoybook < Base

      protected

      def add_sticker(doc, order, offset)
        add_order_id doc, offset, order
        add_logo doc, offset
        add_cut doc, offset

        if order.cash_on_delivery && order.delivery == 'post'
          add_remittance_data doc, order, offset
        end

        if order.delivery == 'post'
          post_data doc, offset, order
        else
          courier_data doc, offset, order
        end

        doc
      end

      def courier_data(doc, offset, order)
        offset_left = offset + 3
        offset_right = offset + 3

        add_cash_on_delivery doc, offset if order.cash_on_delivery

        add_courier_from doc, offset_left
        add_courier_from_phone doc, offset_left

        add_courier_to doc, offset_right
        add_courier_to_where doc, offset_right
        add_courier_to_phone doc, offset_right

        doc.text_box order.username.to_s,
          at: [mm2pt(119), mm2pt(offset_right + 251)],
          width: 240,
          leading: 5,
          size: 16

        doc.text_box order.address.source.to_s,
          at: [mm2pt(118), mm2pt(offset_right + 234)],
          width: 240,
          leading: 9.2

        doc.text_box order.phone.to_s,
          at: [mm2pt(127), mm2pt(offset_right + 210)],
          width: 190
      end

      def add_courier_from(doc, offset_left)
        doc.text_box 'От кого:', at: [mm2pt(5), mm2pt(offset_left + 250)]
        doc.stroke_horizontal_line mm2pt(22), mm2pt(95), at: mm2pt(offset_left + 246)
        doc.text_box 'Enjoybook.ru', at: [mm2pt(23), mm2pt(offset_left + 251)], size: 16
      end

      def add_courier_from_phone(doc, offset_left)
        doc.text_box 'Телефон:', at: [mm2pt(5), mm2pt(offset_left + 242)]
        doc.stroke_horizontal_line mm2pt(26), mm2pt(95), at: mm2pt(offset_left + 238)
        doc.text_box Papers.configuration.phone, at: [mm2pt(27), mm2pt(offset_left + 242)]
      end

      def add_courier_to(doc, offset_right)
        doc.text_box 'Кому:', at: [mm2pt(105), mm2pt(offset_right + 250)]
        doc.stroke_horizontal_line mm2pt(118), mm2pt(205), at: mm2pt(offset_right + 246)
        doc.stroke_horizontal_line mm2pt(118), mm2pt(205), at: mm2pt(offset_right + 238)
      end

      def add_courier_to_where(doc, offset_right)
        doc.text_box 'Куда:', at: [mm2pt(105), mm2pt(offset_right + 234)]
        doc.stroke_horizontal_line mm2pt(117), mm2pt(205), at: mm2pt(offset_right + 230)
        doc.stroke_horizontal_line mm2pt(117), mm2pt(205), at: mm2pt(offset_right + 222)
        doc.stroke_horizontal_line mm2pt(117), mm2pt(205), at: mm2pt(offset_right + 214)
      end

      def add_courier_to_phone(doc, offset_right)
        doc.text_box 'Телефон:', at: [mm2pt(105), mm2pt(offset_right + 210)]
        doc.stroke_horizontal_line mm2pt(126), mm2pt(205), at: mm2pt(offset_right + 206)
      end

      def post_data(doc, offset, order)
        offset_left = offset + 1
        offset_right = offset + 1

        add_track_barcode doc, offset, order
        add_cash_on_delivery doc, offset if order.cash_on_delivery

        add_post_from doc, offset_left
        add_post_from_where doc, offset_left
        add_post_from_phone doc, offset_left
        add_post_from_index doc, offset_left

        add_post_to doc, offset_right
        add_post_to_where doc, offset_right
        add_post_to_index doc, offset_right

        doc.text_box order.username.to_s,
                     at: [mm2pt(119), mm2pt(offset_right + 263)],
                     width: 240,
                     size: 16,
                     leading: 5

        doc.text_box order.address.source.to_s,
                     at: [mm2pt(118), mm2pt(offset_right + 246)],
                     width: 240,
                     leading: 9.2

        set_font doc, 'pechkin.ttf'
        doc.text_box order.address.postcode.to_s,
          size: 32,
          at: [mm2pt(124), mm2pt(offset_right + 219)]
        set_font doc, 'gothic.ttf'

        doc.text_box "Вес: #{order.weight} кг",
          at: [mm2pt(180), mm2pt(offset_right + 209)],
          width: mm2pt(65)
      end

      def add_post_from(doc, offset_left)
        doc.text_box 'От кого:', at: [mm2pt(5), mm2pt(offset_left + 262)]
        doc.stroke_horizontal_line mm2pt(22), mm2pt(95), at: mm2pt(offset_left + 258)
        doc.text_box 'Enjoybook.ru', at: [mm2pt(23), mm2pt(offset_left + 263)], size: 16
      end

      def add_post_from_where(doc, offset_left)
        doc.text_box 'Откуда:', at: [mm2pt(5), mm2pt(offset_left + 254)]
        doc.stroke_horizontal_line mm2pt(22), mm2pt(95), at: mm2pt(offset_left + 250)
        doc.text_box 'г. Оренбург, а/я 2090', at: [mm2pt(23), mm2pt(offset_left + 254)]
      end

      def add_post_from_phone(doc, offset_left)
        doc.text_box 'Телефон:', at: [mm2pt(5), mm2pt(offset_left + 246)]
        doc.stroke_horizontal_line mm2pt(26), mm2pt(95), at: mm2pt(offset_left + 242)
        doc.text_box '8-800-555-11-90', at: [mm2pt(27), mm2pt(offset_left + 246)]
      end

      def add_post_from_index(doc, offset_left)
        doc.text_box 'Индекс:', at: [mm2pt(5), mm2pt(offset_left + 238)]
        doc.bounding_box [mm2pt(23), mm2pt(offset_left + 239)], width: 60, height: 15 do
          doc.stroke_bounds
        end
        doc.text_box '460003', at: [mm2pt(24), mm2pt(offset_left + 238)]
      end

      def add_post_to(doc, offset_right)
        doc.text_box 'Кому:', at: [mm2pt(105), mm2pt(offset_right + 262)]
        doc.stroke_horizontal_line mm2pt(118), mm2pt(205), at: mm2pt(offset_right + 258)
        doc.stroke_horizontal_line mm2pt(118), mm2pt(205), at: mm2pt(offset_right + 250)
      end

      def add_post_to_where(doc, offset_right)
        doc.text_box 'Куда:', at: [mm2pt(105), mm2pt(offset_right + 246)]
        doc.stroke_horizontal_line mm2pt(117), mm2pt(205), at: mm2pt(offset_right + 242)
        doc.stroke_horizontal_line mm2pt(117), mm2pt(205), at: mm2pt(offset_right + 234)
        doc.stroke_horizontal_line mm2pt(117), mm2pt(205), at: mm2pt(offset_right + 226)
      end

      def add_post_to_index(doc, offset_right)
        doc.text_box 'Индекс:', at: [mm2pt(105), mm2pt(offset_right + 218)]
      end

      def add_track_barcode(doc, offset, order)
        track = order.track
        return unless track
        doc.text_box 'ПОЧТА РОССИИ', at: [mm2pt(5), mm2pt(offset + 297)], size: 6

        track_barcode = Papers::Barcodes::TrackBarcode.new.generate track

        doc.image track_barcode.path,
          at: [mm2pt(5), mm2pt(offset + 294.5)],
          fit: [mm2pt(39), mm2pt(9)]

        track_str = [track[0..5], track[6..7], track[8..12], track[13..14]].join '     '
        doc.text_box track_str, at: [mm2pt(7), mm2pt(offset + 285)], size: 8
      end

      def add_cash_on_delivery(doc, offset)
        text = 'Бандероль с объявленной ценностью'
        doc.text_box text, at: [mm2pt(135), mm2pt(offset + 292)],
          size: 14,
          align: :right,
          width: mm2pt(70)
      end

      def add_order_id(doc, offset, order)
        order_id = "#{order.id}#{delivery_mark(order)}"
        doc.text_box order_id,
          at: [mm2pt(155), mm2pt(offset + 297)],
          width: mm2pt(50),
          align: :right
      end

      def add_logo(doc, offset)
        logo = image_path 'logo.png', true
        doc.image logo, fit: [mm2pt(40), mm2pt(25)], at: [mm2pt(88), mm2pt(offset + 297)]
      end

      def add_cut(doc, offset)
        doc.stroke_color = 'cccccc'
        doc.stroke_horizontal_line mm2pt(0), mm2pt(210), at: mm2pt(offset + 200)
        doc.stroke_color = '000000'
      end

      def add_remittance_data(doc, order, offset)
        remittance_without_trifle, remittance_trifle = order.get_money_parts

        txt = order.simple_money_format remittance_without_trifle, remittance_trifle
        doc.text_box txt,
          size: 10,
          at: [mm2pt(135), mm2pt(offset + 280)],
          width: mm2pt(70),
          align: :right

        txt = order.transfer_money_format remittance_without_trifle, remittance_trifle
        doc.text_box "н/п: #{txt}",
          size: 10,
          at: [mm2pt(135), mm2pt(offset + 275)],
          width: mm2pt(70),
          align: :right
      end

      def delivery_mark(order)
        case order.delivery
        when 'post'
          'П'
        when 'oren'
          'О'
        when 'courier'
          'К'
        else
          'О'
        end
      end

    end
  end
end
