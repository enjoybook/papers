module Papers
  module Stickers
    module Formatters
      class A4

        BASE_OFFSETS = { mynamebook: -100, enjoybook: -102 }.freeze

        def page_size
          'A4'
        end

        def offset(index)
          base_offset * pagein_index(index)
        end

        def is_new_page?(index)
          pagein_ind = pagein_index(index)
          pagein_ind.zero? && !index.zero?
        end

        private

        def base_offset
          return @base_offset if @base_offset
          project = Papers.configuration.project_name
          @base_offset = BASE_OFFSETS[project]
        end

        def pagein_index(index)
          index % 3
        end

      end
    end
  end
end
