require 'prawn'

module Papers
  module Stickers
    module Formatters
      class Default

        include Prawn::Measurements

        def page_size
          [mm2pt(210), mm2pt(99)]
        end

        def offset(_)
          base_offset
        end

        def is_new_page?(index)
          !index.zero?
        end

        private

        def base_offset
          -202
        end

      end
    end
  end
end
