require 'prawn'
require 'papers/helpers/file_helpers'
require 'papers/helpers/prawn_helpers'
require 'papers/stickers/formatters/default'
require 'papers/stickers/formatters/a4'

module Papers
  module Stickers
    class Base

      include Papers::FileHelpers
      include Papers::PrawnHelpers
      include Prawn::Measurements

      def generate_file(orders, basename = 'stickers', format: :default)
        doc = generate(orders, format)
        pdf_to_temp(doc, basename)
      end

      def generate_string(orders, format: :default)
        generate(orders, format).render
      end

      protected

      def generate(orders, format)
        formatter = doc_formatter(format)
        doc = create_sticker_document(formatter.page_size)

        orders.each_with_index do |order, index|
          doc.start_new_page if formatter.is_new_page?(index)
          add_sticker doc, order, formatter.offset(index)
        end

        doc
      end

      def create_sticker_document(page_size)
        doc = Prawn::Document.new page_size: page_size, margin: 0
        set_font doc, 'gothic.ttf'
        doc.line_width = 0.1
        doc
      end

      def doc_formatter(format)
        class_name = "Papers::Stickers::Formatters::#{format.to_s.capitalize}"
        Object.const_get(class_name).new
      end

      def add_sticker(doc, order, offset)
        raise NotImplementedError
      end

    end
  end
end
