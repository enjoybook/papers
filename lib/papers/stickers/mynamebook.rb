require 'papers/barcodes/track_barcode'
require 'papers/helpers/qr_helpers'
require 'papers/stickers/base'

module Papers
  module Stickers
    class Mynamebook < Base

      include Papers::QrHelpers

      protected

      def add_sticker(doc, order, offset)
        add_logo doc, offset
        add_cut doc, offset
        seal_place doc, offset

        if order.is_post?
          post_data doc, order, offset
        else
          courier_data doc, order, offset
        end

        add_order_qr doc, order, offset

        doc
      end

      def add_order_qr(doc, order, offset)
        qr = qr_code_image order.qr_url
        size = mm2pt(43)
        doc.image qr, at: [mm2pt(14), mm2pt(offset + 244.5)], fit: [size, size]
      end

      def add_track_barcode_vertical(doc, order, offset)
        track = order.track
        track_barcode = Papers::Barcodes::TrackBarcode.new.generate track

        doc.rotate -90, origin: [mm2pt(13), mm2pt(offset + 240)] do
          doc.image track_barcode.path,
            at: [mm2pt(13), mm2pt(offset + 240)],
            fit: [mm2pt(34), mm2pt(9)]
        end
      end

      def add_track_barcode(doc, order, offset)
        track = order.track
        doc.text_box 'ПОЧТА РОССИИ', at: [mm2pt(5), mm2pt(offset + 297)], size: 6

        track_barcode = Papers::Barcodes::TrackBarcode.new.generate track

        doc.image track_barcode.path,
          at: [mm2pt(5), mm2pt(offset + 293.5)],
          fit: [mm2pt(44), mm2pt(12)]

        track_str = [track[0..5], track[6..7], track[8..12], track[13..14]].join '     '
        doc.text_box track_str, at: [mm2pt(7), mm2pt(offset + 282.2)], size: 8
      end

      def add_order_id(doc, order, x_offset, y_offset)
        order_id = "#{order.id}#{shipping_mark(order)}"

        doc.text_box order_id,
          at: [mm2pt(x_offset), mm2pt(y_offset + 289)]
      end

      def add_logo(doc, offset)
        logo = image_path 'logo.png', true
        doc.image logo,
          at: [mm2pt(66), mm2pt(offset + 296)],
          fit: [mm2pt(60), mm2pt(16)]
      end

      def add_cut(doc, offset)
        doc.stroke_color = 'cccccc'
        doc.stroke_horizontal_line mm2pt(0), mm2pt(210), at: mm2pt(offset + 200)
        doc.stroke_color = '000000'
      end

      def shipping_mark(order)
        case order.shipping
        when 'post'
          'П'
        when 'courier'
          'К'
        else
          'П'
        end
      end

      def seal_place(doc, offset)
        doc.bounding_box [mm2pt(180), mm2pt(offset + 296)], width: 73, height: 94 do
          doc.stroke_bounds
        end
        doc.text_box 'М.П.', at: [mm2pt(187), mm2pt(offset + 284)]
      end

      def post_data(doc, order, offset)
        offset_left = offset + 23
        offset_right = offset + 23

        add_order_id doc, order, 50, offset
        add_track_barcode doc, order, offset
        add_track_barcode_vertical doc, order, offset
        add_post_type doc, order, offset
        add_post_from doc, offset_left
        add_post_to doc, order, offset_right

        add_remittance_data doc, order, offset
        add_post_weight doc, order, offset
      end

      def courier_data(doc, order, offset)
        offset_left = offset + 15
        offset_right = offset + 15

        add_order_id doc, order, 4.5, offset

        if order.cash_on_delivery?
          add_cash_on_delivery doc, offset
          add_remittance_data doc, order, offset
        end

        add_courier_from doc, offset_left
        add_courier_to doc, order, offset_right
      end

      def add_cash_on_delivery(doc, offset)
        doc.text_box 'БАНДЕРОЛЬ',
          at: [mm2pt(130), mm2pt(offset + 296)],
          size: 16
        doc.text_box 'С объявленной ценностью',
          at: [mm2pt(130), mm2pt(offset + 290)],
          size: 10
      end

      def add_post_type(doc, order, offset)
        type = (order.parcel? || order.heavy_parcel?) ? 'ПОСЫЛКА' : 'БАНДЕРОЛЬ'
        doc.text_box type,
          at: [mm2pt(130), mm2pt(offset + 296)],
          size: 16

        add_shipment_details(doc, order, offset)
      end

      def add_shipment_details(doc, order, offset)
        if order.cash_on_delivery?
          doc.text_box 'С объявленной ценностью',
            at: [mm2pt(130), mm2pt(offset + 290)],
            size: 10
          doc.text_box 'Наложным платежом',
            at: [mm2pt(130), mm2pt(offset + 285)],
            size: 10
        elsif !(order.parcel? || order.heavy_parcel?)
          doc.text_box 'ЗАКАЗНАЯ',
            at: [mm2pt(130), mm2pt(offset + 288)],
            size: 16
        end
      end

      def add_post_from(doc, offset)
        x_offset = 0
        add_from_who doc, offset + 250, x_offset
        add_from_where doc, offset + 243, x_offset
        add_from_phone doc, offset + 236, x_offset
        add_from_index doc, offset + 229, x_offset
      end

      def add_post_to(doc, order, offset)
        add_to_who doc, order, offset + 236
        add_to_where doc, order, offset + 221.6
        add_to_index doc,  order, offset + 200
      end

      def add_courier_from(doc, offset)
        add_from_who doc, offset + 250, 0
        add_from_phone doc, offset + 243, 0
      end

      def add_courier_to(doc, order, offset)
        add_to_who doc, order, offset + 236
        add_to_where doc, order, offset + 221.6
        add_to_phone doc, order, offset + 200
      end

      def add_from_who(doc, offset, x_offset)
        doc.text_box 'От кого:', at: [mm2pt(x_offset + 5), mm2pt(offset)]
        doc.stroke_horizontal_line mm2pt(x_offset + 23), mm2pt(85), at: mm2pt(offset - 4)
        doc.text_box 'Mynamebook.ru', at: [mm2pt(x_offset + 24), mm2pt(offset + 1)], size: 16
      end

      def add_from_where(doc, offset, x_offset)
        doc.text_box 'Откуда:', at: [mm2pt(x_offset + 5), mm2pt(offset)]
        doc.stroke_horizontal_line mm2pt(x_offset + 22), mm2pt(85), at: mm2pt(offset - 4)
        doc.text_box 'г. Оренбург, а/я 2090',
          at: [mm2pt(x_offset + 23), mm2pt(offset)],
          width: mm2pt(70),
          leading: 10
      end

      def add_from_phone(doc, offset, x_offset)
        doc.text_box 'Телефон:', at: [mm2pt(x_offset + 5), mm2pt(offset)]
        doc.stroke_horizontal_line mm2pt(x_offset + 26), mm2pt(85), at: mm2pt(offset - 4)
        doc.text_box Papers.configuration.phone, at: [mm2pt(x_offset + 27), mm2pt(offset)]
      end

      def add_from_index(doc, offset, x_offset)
        doc.text_box 'Индекс:', at: [mm2pt(x_offset + 5), mm2pt(offset)]
        doc.bounding_box [mm2pt(x_offset + 24), mm2pt(offset + 1)], width: 60, height: 15 do
          doc.stroke_bounds
        end
        doc.text_box '460003', at: [mm2pt(x_offset + 25), mm2pt(offset)]
      end

      def add_to_who(doc, order, offset)
        doc.text_box 'Кому:', at: [mm2pt(105), mm2pt(offset)]
        doc.stroke_horizontal_line mm2pt(118), mm2pt(206), at: mm2pt(offset - 4)
        doc.stroke_horizontal_line mm2pt(118), mm2pt(206), at: mm2pt(offset - 11.2)

        doc.text_box order.username.to_s,
          at: [mm2pt(119), mm2pt(offset + 1)],
          width: mm2pt(84),
          size: 16,
          leading: 5
      end

      def add_to_where(doc, order, offset)
        doc.text_box 'Куда:', at: [mm2pt(105), mm2pt(offset)]
        doc.stroke_horizontal_line mm2pt(118), mm2pt(206), at: mm2pt(offset - 4)
        doc.stroke_horizontal_line mm2pt(118), mm2pt(206), at: mm2pt(offset - 11.2)
        doc.stroke_horizontal_line mm2pt(118), mm2pt(206), at: mm2pt(offset - 18.2)

        doc.text_box order.address.source.to_s,
          at: [mm2pt(119), mm2pt(offset)],
          width: mm2pt(84),
          leading: 10
      end

      def add_to_phone(doc, order, offset)
        doc.text_box 'Телефон:', at: [mm2pt(105), mm2pt(offset)]
        doc.stroke_horizontal_line mm2pt(126), mm2pt(205), at: mm2pt(offset - 4)

        doc.text_box order.phone.to_s,
          at: [mm2pt(127), mm2pt(offset)],
          width: 190
      end

      def add_to_index(doc, order, offset)
        doc.text_box 'Индекс:', at: [mm2pt(105), mm2pt(offset)]

        set_font doc, 'pechkin.ttf'
        doc.text_box order.address.postcode.to_s, at: [mm2pt(125), mm2pt(offset)], size: 24
        set_font doc, 'gothic.ttf'
      end

      def add_remittance_data(doc, order, offset)
        remittance_without_trifle, remittance_trifle = remittance(order)

        txt = order.simple_money_format remittance_without_trifle, remittance_trifle
        doc.text_box "о/ц: #{txt}",
          at: [mm2pt(118), mm2pt(offset + 278)],
          size: 11,
          width: mm2pt(65)
        doc.text_box "н/п: #{txt}",
          at: [mm2pt(118), mm2pt(offset + 270)],
          size: 11,
          width: mm2pt(65)

        txt = order.transfer_money_format remittance_without_trifle, remittance_trifle
        doc.text_box "(#{txt})",
          at: [mm2pt(118), mm2pt(offset + 274)],
          size: 8,
          width: mm2pt(65)
        doc.text_box "(#{txt})",
          at: [mm2pt(118), mm2pt(offset + 266)],
          size: 8,
          width: mm2pt(65)
      end

      def add_post_weight(doc, order, offset)
        doc.text_box "Вес: #{order.weight} кг",
          at: [mm2pt(105), mm2pt(offset + 210)],
          width: mm2pt(65)
      end

      def remittance(order)
        if order.cash_on_delivery?
          order.get_money_parts
        else
          [0, 0]
        end
      end

    end
  end
end
