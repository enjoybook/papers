require 'papers/paper_factory'
require 'papers/pony_outsource_registers/mynamebook'

module Papers
  class PonyOutsourceRegister

    class << self
      def new
        Papers::PaperFactory.build('PonyOutsourceRegisters')
      end
    end

  end
end
