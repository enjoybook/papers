require 'papers/pony_courier_registers/mynamebook'

module Papers
  module PonyOutsourceRegisters
    class Mynamebook < Papers::PonyCourierRegisters::Mynamebook

      START_ROW          = 4
      SECOND_ROW         = 1
      CELLS              = { first: 0, second: 1, third: 2 }.freeze
      GAPS               = { four: 4, five: 5, six: 6 }.freeze
      ORDER_SEATS_NUMBER = 1
      ORDER_WEIGHT       = 0.5
      TEMPLATE           = 'pony_courier_delivery.xlsx'.freeze
      OUTPUT_BASENAME    = 'pony_courier_delivery'.freeze
      DELIVERY_SERVICE   = 2

      def generate_file(orders, params = {}, basename = 'pony_outsource')
        workbook = open_template_table
        add_register_number(workbook[0], params[:register_number])

        data = prepare_data orders, sender_info(params[:outsource])
        add_content workbook, data

        add_document_footer(workbook[0], orders.size)
        xlsx_to_temp(workbook, basename)
      end

      private

      def prepare_data(orders, sender_info)
        orders.map! do |order|
          order_id = "#{order.id}mb"
          recipient_city = order.address.city.nil? ? order.address.settlement : order.address.city
          {
            date_pick_up_order: Time.now.strftime('%d.%m.%Y'),
            track_number:       order.track,
            sender_country:     'Россия',
            sender_city:        'Москва',
            sender_address:     sender_info[:sender_address],
            order_id:           order_id,
            recipient_fullname: order.username,
            recipient_country:  order.address.country,
            recipient_city:     recipient_city,
            recipient_address:  order.address.source,
            order_seats_number: ORDER_SEATS_NUMBER,
            order_weight:       ORDER_WEIGHT,
            control_weight:     '',
            service:            DELIVERY_SERVICE
          }
        end
      end

      def add_register_number(worksheet, register_number)
        cell = worksheet.add_cell(SECOND_ROW, CELLS[:second], "№ #{register_number}")
        cell.change_font_bold(true)
        cell.change_font_color('FF0000') # red
        cell.change_font_size(14)
      end

      def add_document_footer(worksheet, rows_size)
        start_row = rows_size + START_ROW
        5.times { worksheet.add_cell(start_row, CELLS[:first], '') }

        worksheet.add_cell(start_row + GAPS[:four], CELLS[:third], 'Отпустил: _____________________________')
        worksheet.add_cell(start_row + GAPS[:six], CELLS[:third], 'Принял: _____________________________')
        worksheet.add_cell(start_row + GAPS[:six], CELLS[:third] + GAPS[:five], 'Количество мест: _____________________________')
      end
    end
  end
end
