require 'prawn'
require 'papers/helpers/prawn_helpers'
require 'papers/helpers/file_helpers'

module Papers
  class Magnet

    include Papers::FileHelpers
    include Papers::PrawnHelpers

    PAGE_SIZE = 6

    def generate_file(orders, basename = 'magnet')
      doc = generate(orders)
      pdf_to_temp(doc, basename)
    end

    def generate_string(orders)
      generate(orders).render
    end

    private

    def generate(orders)
      order_mosaics = mosaics(orders)

      doc = Prawn::Document.new page_size: 'A4', margin: 0

      order_mosaics.each_with_index do |element, index|
        add_mosaic(doc, element, index)
      end

      doc
    end

    def add_mosaic(doc, element, index)
      mosaic_file = element[:file]
      order_id = element[:id]

      page_index = index % PAGE_SIZE
      doc.start_new_page if page_index.zero? && !index.zero?

      position = magnets_coords[page_index]

      render_order_id(doc, order_id, position, index)

      mosaic_x = position[0] + magnets_config[:padding]
      mosaic_y = position[1] + magnets_config[:padding]
      bound(doc, mosaic_file, mosaic_x, mosaic_y,
            magnets_config[:mosaic_width], magnets_config[:mosaic_height])

      bottom_left_dot(doc, position)
      bottom_right_dot(doc, position)
      top_left_dot(doc, position)
      top_right_dot(doc, position)
    end

    def mosaics(orders)
      orders.reduce [] do |memo, order|

        order.all_books.each do |book|
          memo.push(file: open(book.magnets_mosaic.url), id: order.id)
        end

        memo
      end
    end

    def render_order_id(doc, order_id, pos, index)
      if index.even?
        order_id_x = pos[0] - magnets_config[:left] / 4
        order_id_y = pos[1] + 206

        doc.draw_text order_id, at: [order_id_x, order_id_y], rotate: 90
      else
        order_id_x = pos[0] + magnets_config[:mosaic_height] + magnets_config[:padding] * 2 + magnets_config[:left] / 1.5
        order_id_y = pos[1] + 206

        doc.draw_text order_id, at: [order_id_x, order_id_y], rotate: 90
      end
    end

    def bottom_left_dot(doc, position)
      draw_dot(doc, position[0], position[1])
    end

    def bottom_right_dot(doc, position)
      y = position[1] + magnets_config[:mosaic_width] + magnets_config[:padding] * 2
      draw_dot(doc, position[0], y)
    end

    def top_left_dot(doc, position)
      x = position[0] + magnets_config[:mosaic_height] + magnets_config[:padding] * 2
      draw_dot(doc, x, position[1])
    end

    def top_right_dot(doc, position)
      x = position[0] + magnets_config[:mosaic_height] + magnets_config[:padding] * 2
      y = position[1] + magnets_config[:mosaic_width] + magnets_config[:padding] * 2
      draw_dot(doc, x, y)
    end

    def magnets_coords
      [
        [ # X, Y
          magnets_config[:left],
          magnets_config[:bottom] + magnets_config[:mosaic_width] * 2 + magnets_config[:padding] * 4
        ],
        [
          magnets_config[:left] + magnets_config[:mosaic_height] + magnets_config[:padding] * 2,
          magnets_config[:bottom] + magnets_config[:mosaic_width] * 2 + magnets_config[:padding] * 4
        ],
        [
          magnets_config[:left],
          magnets_config[:bottom] + magnets_config[:mosaic_width] + magnets_config[:padding] * 2
        ],
        [
          magnets_config[:left] + magnets_config[:mosaic_height] + magnets_config[:padding] * 2 ,
          magnets_config[:bottom] + magnets_config[:mosaic_width] + magnets_config[:padding] * 2
        ],
        [
          magnets_config[:left],
          magnets_config[:bottom]
        ],
        [
          magnets_config[:left] + magnets_config[:mosaic_height] + magnets_config[:padding] * 2,
          magnets_config[:bottom]
        ]
      ]
    end

    def magnets_config
      {
        mosaic_width: 261.85,
        mosaic_height: 260.74,

        # padding between mosaic image and dots
        padding: 7.78 + 0.85,

        # side paddings
        left: 18,
        bottom: 4.5
      }
    end

    def draw_dot(doc, x, y)
      doc.stroke_rectangle [x, y], 0.85, 0.85
    end
  end
end
