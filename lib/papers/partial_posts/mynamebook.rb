# frozen_string_literal: true

require 'csv'

require 'papers/helpers/base_helpers'

module Papers
  module PartialPosts
    class Mynamebook

      include Papers::BaseHelpers
      include Papers::FileHelpers

      ADDRESSTYPE = 'стандартный'

      #
      # 1) идентификатор
      # - NUM  Numeric
      #
      # 2) почтовый индекс
      # - INDEXTO  Char(6)
      #
      # 3) наименование страны РТМ 0002 (для
      # МПО) - COUNTRY Integer
      #
      # 4) тип адреса
      # -ADDRESSTYPE   Integer
      #
      # 5) номер типа адреса
      # -NUMADDRESSTYPE Char(15)
      #
      # 6) регион
      # -REGION Char(200)
      #
      # # 7) район
      # -AREA Char(200)
      #
      # 8) нас.пункт
      # -CITY Char(200)
      #
      # 9) внутригородской элемент (мкр,
      #   поселение)   -LOCATION Char(200)
      #
      # 10) улица
      # -STREET Char(200)
      #
      # 11) номер здания
      # -HOUSE Char(60)
      #
      # 12) литера
      # -LETTER Char(2)
      #
      # 13) дробь
      # -SLASH Char(8)
      #
      # 14) корпус
      # -CORPUS Char(8)
      #
      # 15) строение
      # -BUILDING Char(8)
      #
      # 16) наименование гостиницы
      # -HOTEL Char(50)
      #
      # 17) номер помещения (кв, офис)
      # -ROOM Char(8)
      #
      # 18) получатель РПО
      # -ADRESAT Char(147)
      #
      # 19) вес отправления
      # -MASS (######.###)
      #
      #   20) длина отправления
      # -VLENGTH  - Integer
      #
      # 21) ширина отправления
      # -VWIDTH  - Integer
      #
      # 22) высота отправления
      # -VHEIGHT  - Integer
      #
      # 23) объявленная ценность
      # -VALUE (######.##)
      #
      #   24) наложенный платеж
      # -PAYMENT (######.##)
      #
      #   25) инф. пользавателя
      # -COMMENT Char(256)
      #
      # 26) данные телефона получателя
      # -TELADDRESS Char(20)
      #
      # 27) номер договора
      # -NUM_CONTRACT Char(10)
      #
      # 28) ИНН фактического отправителя
      # -INNFACT
      #
      # 29) КПП фактического отправителя
      # -KPPFACT
      #
      # 30) Код подразд. фактического отправителя
      # -DEPCODEFACT
      #
      # 31) APONUM
      #
      # 32) Код типа SMS-уведомлений
      # -SMSNOTICER

      def generate_file(orders, basename = 'partial_post')
        csv = generate_string(orders)
        write_to_temp(csv, [basename, '.csv'])
      end

      def generate_string(orders)
        result = CSV.generate col_sep: ';' do |csv|
          add_header(csv)
          orders.each { |order| add_row(csv, order) }
        end

        result.encode('cp1251')
      end

      private

      def add_header(csv)
        csv << [
          'NUM',
          'INDEXTO',

          'ADDRESSTYPE',
          'NUMADDRESSTYPE',

          'COUNTRY',
          'REGION',
          'AREA',
          'CITY',

          'LOCATION',
          'STREET',
          'HOUSE',
          'LETTER',
          'SLASH',
          'CORPUS',
          'BUILDING',
          'HOTEL',
          'ROOM',

          'ADRESAT',

          'MASS',
          'VLENGTH',
          'VWIDTH',
          'VHEIGHT',

          'VALUE',
          'PAYMENT',

          'COMMENT',
          'TELADDRESS',

          'NUM_CONTRACT',
          'INNFACT',
          'KPPFACT',
          'DEPCODEFACT',
          'APONUM',
          'SMSNOTICER'
        ]
      end

      def add_row(csv, order)
        csv << order2row(order)
      end

      def order2row(order)
        address = order.address
        [
          order.track,
          address&.postcode,

          ADDRESSTYPE,
          nil,

          address&.country,
          address&.region_full,
          address&.area_full,
          address&.locality_full,

          nil,
          address&.street_full,
          address&.house,
          nil,
          nil,
          nil,
          nil,
          nil,
          address&.flat,

          order.username,
          order.weight,
          nil,
          nil,
          nil,

          cash_on_delivery_amount(order),
          cash_on_delivery_amount(order),

          order.id,
          nil,

          nil,
          nil,
          nil,
          nil,
          nil,
          nil

        ].map { |el| blank?(el) ? nil : el }
      end

      def cash_on_delivery_amount(order)
        amount = order.cash_on_delivery ? order.amount : 0
        '%.2f' % amount if amount
      end

    end
  end
end
