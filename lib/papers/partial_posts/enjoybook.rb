require 'csv'
require 'papers/helpers/base_helpers'

module Papers
  module PartialPosts
    class Enjoybook

      include Papers::BaseHelpers
      include Papers::FileHelpers

      def generate_file(orders, basename = 'partial_post')
        csv = generate_string(orders)
        write_to_temp(csv, [basename, '.csv'])
      end

      def generate_string(orders)
        result = CSV.generate col_sep: ';' do |csv|
          add_header(csv)

          orders.each do |order|
            row = order2row(order).map { |el| blank?(el) ? nil : el }
            csv << row
          end
        end
        result.encode 'cp1251'
      end

      private

      def add_header(csv)
        csv << [
          'NUM',
          'INDEXTO',
          'REGION',
          'AREA',
          'CITY',
          'ADRES',
          'ADRESAT',

          'MASS',
          'VLENGTH', 'VWIDTH', 'VHEIGHT',

          'VALUE', # объявленная ценность
          'PAYMENT', # наложенный платеж

          'ADDRESSTYPE',
          'NUMADDRESSTYPE',
          'LOCATION',
          'STREET',

          'HOUSE', 'LETTER', 'SLASH', 'CORPUS', 'BUILDING', 'HOTEL', 'ROOM',
          'COMMENT'
        ]
      end

      def order2row(order)
        address = order.address

        [
          order.track,
          address&.postcode,
          address&.region_full,
          address&.area_full,
          address&.locality_full,
          nil,
          order.username,

          calculate_weight(order),
          0, 0, 0,

          0,
          cash_on_delivery_amount(order),

          1,
          nil,
          nil,
          address&.street_full,

          address&.house, nil, nil, nil, nil, nil, address&.flat,
          order.id
        ]
      end

      def cash_on_delivery_amount(order)
        if order.cash_on_delivery
          rub, kop = order.calc_post_remittance_amount
          "#{rub}.#{kop}".to_f
        else
          0
        end
      end

      def calculate_weight(order)
        full_weight = pack_weight + books_weight(order)
        full_weight.round(3)
      end

      def pack_weight
        0.135
      end

      def books_weight(order)
        order.all_books.reduce(0) do |weight, book|
          weight + book_weight(book)
        end
      end

      def book_weight(book)
        book_base_weight = 0.2584 # 20 стр
        page_weight = 0.00437

        book_base_weight + page_weight * (book.pages_count - 20)
      end

    end
  end
end
