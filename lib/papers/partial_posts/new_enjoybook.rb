require 'csv'
require 'papers/helpers/base_helpers'

module Papers
  module PartialPosts
    class NewEnjoybook

      include Papers::BaseHelpers
      include Papers::FileHelpers

      ADDRESSTYPE = 'стандартный'

      def generate_file(orders, basename = 'partial_post')
        csv = generate_string(orders)
        write_to_temp(csv, [basename, '.csv'])
      end

      def generate_string(orders)
        result = CSV.generate col_sep: ';' do |csv|
          add_header(csv)

          orders.each do |order|
            row = order2row(order).map { |el| blank?(el) ? nil : el }
            csv << row
          end
        end
        result.encode 'cp1251'
      end

      private

      def add_header(csv)
        csv << [
          'NUM',
          'INDEXTO',

          'ADDRESSTYPE',
          'NUMADDRESSTYPE',

          'COUNTRY',
          'REGION',
          'AREA',
          'CITY',

          'LOCATION',
          'STREET',
          'HOUSE',
          'LETTER',
          'SLASH',
          'CORPUS',
          'BUILDING',
          'HOTEL',
          'ROOM',

          'ADRESAT',

          'MASS',
          'VLENGTH',
          'VWIDTH',
          'VHEIGHT',

          'VALUE',
          'PAYMENT',

          'COMMENT',
          'TELADDRESS',

          'NUM_CONTRACT',
          'INNFACT',
          'KPPFACT',
          'DEPCODEFACT',
          'APONUM',
          'SMSNOTICER',
        ]
      end

      def order2row(order)
        address = order.address

        [
          order.track,
          address&.postcode,

          ADDRESSTYPE,
          nil,

          address&.country,
          address&.region_full,
          address&.area_full,
          address&.locality_full,

          nil,
          address&.street_full,
          address&.house,
          nil,
          nil,
          nil,
          nil,
          nil,
          address&.flat,

          order.username,
          order.weight,
          nil,
          nil,
          nil,

          cash_on_delivery_amount(order),
          cash_on_delivery_amount(order),

          order.id,
          nil,

          nil,
          nil,
          nil,
          nil,
          nil,
          nil
        ]
      end

      def cash_on_delivery_amount(order)
        amount = order.cash_on_delivery ? order.amount : 0
        '%.2f' % amount if amount
      end

    end
  end
end
