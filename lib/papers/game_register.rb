require 'papers/helpers/axlsx_helpers'

module Papers
  class GameRegister

    START_ROW = 5
    QR_CODE_CELL = 5
    LINE_ITEM_DESKTOP_PREFIX = '77777'

    include Papers::QrHelpers
    include Papers::AxlsxHelpers

    def generate_file(orders, basename = 'games')
      create_xlsx_file(basename) do |sheet|
        plan_title(sheet)
        plan_summary(sheet, orders)
        plan_content(sheet, orders)
        plan_settings(sheet)
      end
    end

    private

    def plan_title(sheet)
      title = "План на отправку игр #{DateTime.now.strftime '%e.%m.%y'}"
      title_styles = sheet.styles.add_style(
        sz: 16,
        alignment: { horizontal: :center },
        wrap_text: true
      )

      sheet.merge_cells 'A1:F1'
      sheet.add_row [title], style: title_styles, height: 16
    end

    def plan_summary(sheet, orders)
      sheet.add_row

      sheet.add_row ['', 'Всего заказов', orders.size]
      sheet.add_row ['', 'Всего игр', orders.map(&:games).flatten.size]

      sheet.add_row
    end

    def plan_content(sheet, orders)
      row_styles = sheet.styles.add_style(
        alignment: { vertical: :center, horizontal: :center },
        wrap_text: true
      )

      sheet.add_row ['№', 'Заказ', 'Кол-во игр', 'Заказчик', 'Доставка', 'Qr-код']

      orders.each_with_index do |order, i|
        index = i + 1

        row = [
          index,
          order.id,
          order.games.size,
          order.username,
          order.shipping
        ]

        sheet.add_row row, types: [:string], style: row_styles, height: 90
        print_qr_code(sheet, order, [QR_CODE_CELL, START_ROW + index])
      end
    end

    def plan_settings(sheet)
      sheet.column_widths 3, 13, 13, 40, 10, 15
    end

    def print_qr_code(sheet, order, at)
      # To prevent removing tempfiles by garbage collector
      # https://enjoybook.atlassian.net/browse/MNB-225
      @qrcodes ||= []

      link = qr_link(order)
      @qrcodes << qr_code_image(link)

      sheet.add_image(image_src: @qrcodes.last.path) do |img|
        img.width = 95
        img.height = 95
        img.start_at *at
      end
    end

    def qr_link(order)
      game = order.games.first
      "http://mynamebook.ru/qr/#{LINE_ITEM_DESKTOP_PREFIX}#{game.id}/book" if game
    end

  end
end
