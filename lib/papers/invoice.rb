# TODO: fix product name
require 'russian'
require 'sablon'
require 'date'
require 'papers/helpers/base_helpers'
require 'papers/helpers/file_helpers'

module Papers
  class Invoice

    include Papers::BaseHelpers
    include Papers::FileHelpers

    MIN_TABLE_ROWS = 6

    # @param [Hash] params
    # @option opts [Integer] :invoices_count
    # @option opts [Integer] :guarantees_count
    def initialize(params = {})
      @invoices_count = params[:invoices_count] || 1
      @guarantees_count = params[:guarantees_count] || 1
    end

    def generate_file(orders, basename = 'invoice')
      sablon_template = ::Sablon.template template_path('invoice.docx', true)
      context = { items: template_params(orders) }
      tempfile = ::Tempfile.new [basename, '.docx']
      sablon_template.render_to_file(tempfile, context)
      tempfile
    end

    def generate_string(orders)
      sablon_template = ::Sablon.template template_path('invoice.docx', true)
      context = { items: template_params(orders) }
      sablon_template.render_to_string(context)
    end

    private

    def template_params(orders)
      orders.map.each_with_index do |order, index|
        invoice_params = to_invoice_params(order)
        guarantee_params = to_guarantee_params(order)

        invoices = Array.new(@invoices_count, invoice_params)
        guarantees = Array.new(@guarantees_count, guarantee_params)

        is_last_order = last?(index, orders.count)
        guarantees.last[:break_page] = false if is_last_order

        {
          invoices: invoices,
          guarantees: guarantees,
        }
      end
    end

    def last?(index, size)
      false unless index == size - 1
    end

    def empty_rows(order)
      rows_count = [0, MIN_TABLE_ROWS - order.line_items.size].max
      Array.new(rows_count, true)
    end

    def to_invoice_params(order)
      country = order.address && order.address.country
      address = format_address(order.address)
      city_and_postcode = format_city_and_postcode(order.address)
      {
        username: str_with_translit(order.username),
        address: str_with_translit(address),
        city_and_postcode: str_with_translit(city_and_postcode),
        country: str_with_translit(country),
        line_items: order.line_items,
        line_items_count: order.line_items_count,
        phone: order.phone,
        total: order.total.round,
        empty_rows: empty_rows(order)
      }
    end

    def to_guarantee_params(order)
      {
        number: order.track,
        date: order.predict_sending_time.strftime('%d.%m.%Y'),
        country: order.address && order.address.country,
        products: items_name_and_count_text(order.description_items_by_type),
        break_page: true
      }
    end

    def items_name_and_count_text(description)
      description[:count_items_by_type].map do |product_name, items_count|
        " #{product_name} в количестве #{items_count} шт."
      end
    end

    def format_city_and_postcode(address)
      return unless address

      result = []
      result << address.city if address.city
      result << address.postcode if address.postcode
      result.join(', ')
    end

    def format_address(address)
      return unless address

      result = []
      result << address.street if address.street
      result << address.house if address.house
      result << address.flat if address.flat
      result.join(', ')
    end

    def str_with_translit(str)
      return if blank?(str)
      [str, ::Russian.translit(str)].join(' / ')
    end

  end
end
