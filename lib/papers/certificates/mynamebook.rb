require 'prawn'
require 'papers/helpers/file_helpers'
require 'papers/helpers/prawn_helpers'

module Papers
  module Certificates
    class Mynamebook

      include Papers::FileHelpers
      include Papers::PrawnHelpers
      include Prawn::Measurements

      def generate_file(certificate, basename = 'certificate')
        doc = generate(certificate)
        pdf_to_temp(doc, basename)
      end

      def generate_string(certificate)
        generate(certificate).render
      end

      private

      def generate(certificate)
        doc = Prawn::Document.new page_size: 'A5', margin: 0, page_layout: :landscape,
          skip_page_creation: true

        certificate.promos.each do |promo|
          doc.start_new_page
          set_font doc, 'pfcentroslabprobold.ttf'
          add_image doc
          doc.fill_color 'ffffff'
          add_promo doc, promo
        end

        doc
      end

      def add_image(doc)
        mockup_path = image_path 'certificate.jpg', true
        doc.image mockup_path, fit: [mm2pt(213), mm2pt(150)], at: [mm2pt(0), mm2pt(150)]
      end

      def add_promo(doc, promo)
        offset = 100
        promo.code.split('').each_with_index do |digit, index|
          doc.text_box digit,
            at: [mm2pt(offset + index * 10), mm2pt(62.5)],
            width: mm2pt(20), size: 36
        end
      end

    end
  end
end
