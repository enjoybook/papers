require 'prawn'
require 'papers/helpers/prawn_helpers'
require 'papers/helpers/file_helpers'

module Papers
  module Certificates
    class Enjoybook
      extend Prawn::Measurements
      include Papers::FileHelpers
      include Papers::PrawnHelpers
      include Prawn::Measurements

      MOCKUP_PARAMS = {
        casual: { image: 'certificates/casual.jpg', color: '000000' },
        birthday: { image: 'certificates/birthday.jpg', color: '000000' },
        'new-year': { image: 'certificates/newyear.jpg', color: '000000' },
        tema: { image: 'certificates/tema.jpg', color: '000000' },
        default: { image: 'certificates/casual.jpg', color: '000000' }
      }.freeze

      WIDTH = mm2pt(148)
      HEIGHT = mm2pt(148)

      def generate_file(certificate, basename = 'certificate')
        doc = generate(certificate)
        pdf_to_temp(doc, basename)
      end

      def generate_string(certificate)
        generate(certificate).render
      end

      private

      def generate(certificate)
        doc = create_document
        add_certificate(doc, certificate)
        doc
      end

      def create_document
        doc = Prawn::Document.new(page_size: [WIDTH, HEIGHT], margin: 0, page_layout: :landscape)
        set_font(doc, 'intro-light.ttf')
        doc
      end

      def add_certificate(doc, certificate)
        render_background(doc, certificate)
        render_text(doc, certificate)
      end

      def render_background(doc, certificate)
        image = params(certificate)[:image]
        mockup_path = image_path(image, true)
        doc.image(mockup_path, fit: [WIDTH, HEIGHT], at: [mm2pt(0), HEIGHT])
      end

      def render_text(doc, certificate)
        doc.text_box "#{certificate.price} рублей",
                     at: [mm2pt(59), mm2pt(57)], width: mm2pt(80), align: :center

        doc.text_box promo_code(certificate),
                     at: [mm2pt(48), mm2pt(44)], width: mm2pt(80), align: :center, size: 18
      end

      def params(certificate)
        MOCKUP_PARAMS[certificate.mockup.to_sym] || MOCKUP_PARAMS[:default]
      end

      def promo_code(certificate)
        certificate.promo.code
      end
    end
  end
end
