require 'papers/paper_factory'
require 'papers/certificates/enjoybook'
require 'papers/certificates/mynamebook'

module Papers
  class Certificate

    class << self
      def new
        Papers::PaperFactory.build('Certificates')
      end
    end

  end
end
