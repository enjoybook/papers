require 'papers/paper_factory'
require 'papers/partial_posts/enjoybook'
require 'papers/partial_posts/new_enjoybook'
require 'papers/partial_posts/mynamebook'

module Papers
  class PartialPost

    class << self
      def new(new_version: false)
        Papers::PaperFactory.build('PartialPosts', new_version: new_version)
      end
    end

  end
end
