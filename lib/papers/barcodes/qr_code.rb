require 'barby'
require 'barby/barcode'
require 'barby/barcode/qr_code'
require 'barby/outputter/ascii_outputter'
require 'barby/outputter/png_outputter'

module Papers
  module Barcodes
    class QrCode

      LEVEL = :m
      SIZE = 5

      def initialize(text)
        @text = text
      end

      def to_ascii
        code_instance(@text).to_ascii
      end

      def to_png
        code_instance(@text).to_image(xdim: 5)
      end

      def to_file
        file = Tempfile.new(['qrcode', '.png'])
        to_png.save file.path
        file
      end

      private

      def code_instance(text)
        Barby::QrCode.new text, level: LEVEL, size: SIZE
      end

    end
  end
end
