require 'barby'
require 'barby/barcode/code_25_interleaved'
require 'barby/outputter/png_outputter'

module Papers
  module Barcodes
    class TrackBarcode

      def generate(track)
        barcode = Barby::Code25Interleaved.new track
        barcode.wide_width = 2
        write_to_file(barcode)
      end

      private

      def write_to_file(barcode)
        tempfile = Tempfile.new ['barcode', '.png']

        File.open(tempfile.path, 'wb') do |f|
          f.write barcode.to_png(height: 25, margin: 0)
        end

        tempfile
      end

    end
  end
end
