require 'papers/pony_courier_registers/base'
require 'papers/pony_courier_registers/senders/enjoybook'

module Papers
  module PonyCourierRegisters
    class Enjoybook < Base
      CONTRACT_CODE = '6-775'.freeze
      INTENATIONAL_CONTRACT_CODE = '6-798'.freeze

      protected

      def sender_info(_)
        Senders::Enjoybook.info
      end

      def prepare_data(entities, sender_info)
        entities.map do |entity|
          is_certificate = entity.class.name == 'Certificate'
          prefix = is_certificate ? 'c-' : 'o-'
          entity_id = "#{prefix}#{entity.id}eb"

          {
            contract_code: international_sending?(entity) ? CONTRACT_CODE : INTENATIONAL_CONTRACT_CODE,
            date_pick_up_order: DateTime.now.strftime('%d.%m.%Y'),
            email: entity.email,
            company: 'Enjoybook.ru',
            sender_country: sender_info[:sender_country],
            sender_region: sender_info[:sender_region],
            sender_area: sender_info[:sender_area],
            sender_city: sender_info[:sender_city],
            sender_address: sender_info[:sender_address],
            sender_postcode: sender_info[:sender_postcode],
            sender_email: sender_info[:sender_email],
            sender_phone: sender_info[:sender_phone],
            sender_person: sender_info[:sender_person],
            sender_note: sender_info[:sender_note],
            payment_type: entity.cash_on_delivery ? '1' : '2',
            order_id: entity_id,
            order_created_at: entity.created_at.strftime('%d.%m.%Y'),
            authority: 'нет',
            shipment_type: 'коробка',
            package_length: '240',
            package_width: '240',
            package_height: '40',
            dangerous_freight: 'нет',
            delivery_automatic_email_at: Papers.configuration.email,
            recipient_fullname: entity.username,
            recipient_country: entity.address.country,
            recipient_region: entity.address.region,
            recipient_area: entity.address.area,
            recipient_city: entity.address.city.nil? ? entity.address.settlement : entity.address.city,
            recipient_address: entity.address.source,
            recipient_postalcode: entity.address.postcode,
            recipient_email: entity.email,
            recipient_phone: entity.phone,
            recipient_address_note: '',
            delivery_terms: '4',
            description_contents: 'изделие',
            order_seats_number: '1',
            order_weight: '0.5',
            letter_guarantee: 'нет',
            amount: '0',
            product_title: 'фотоизделие',
            product_articul: entity_id,
            product_quantity: is_certificate ? entity.quantity : entity.line_items.size,
            product_price: nil,
            product_cost: entity.cash_on_delivery ? entity.amount : '0',
            product_delivery_cost: '0'
          }
        end
      end
    end
  end
end
