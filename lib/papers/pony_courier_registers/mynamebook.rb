require 'papers/pony_courier_registers/base'
require 'papers/pony_courier_registers/senders/mynamebook'

module Papers
  module PonyCourierRegisters
    class Mynamebook < Base

      CONTRACT_CODE = '6-776'.freeze
      INTENATIONAL_CONTRACT_CODE = '6-799'.freeze

      protected

      def sender_info(sender_name)
        Senders::Mynamebook.info(sender_name)
      end

      def prepare_data(orders, sender_info)
        orders.map do |order|
          books_size = order.total_books.size
          product_quantity = books_size.zero? ? 1 : books_size

          order_id = "#{order.id}mb"
          {
            contract_code: international_sending?(order) ? CONTRACT_CODE : INTENATIONAL_CONTRACT_CODE,
            date_pick_up_order: DateTime.now.strftime('%d.%m.%Y'),
            email: order.email,
            company: 'Mynamebook.ru',
            sender_country: sender_info[:sender_country],
            sender_region: sender_info[:sender_region],
            sender_area: sender_info[:sender_area],
            sender_city: sender_info[:sender_city],
            sender_address: sender_info[:sender_address],
            sender_postcode: sender_info[:sender_postcode],
            sender_email: sender_info[:sender_email],
            sender_phone: sender_info[:sender_phone],
            sender_person: sender_info[:sender_person],
            sender_note: sender_info[:sender_note],
            payment_type: order.cash_on_delivery ? '1' : '2',
            order_id: order_id,
            order_created_at: order.created_at.strftime('%d.%m.%Y'),
            authority: 'нет',
            shipment_type: 'коробка',
            package_length: '300',
            package_width: '300',
            package_height: '40',
            dangerous_freight: 'нет',
            delivery_automatic_email_at: 'enjoybook.ru@gmail.com',
            recipient_fullname: order.username,
            recipient_country: order.address.country,
            recipient_region: order.address.region,
            recipient_area: order.address.area,
            recipient_city: order.address.city.nil? ? order.address.settlement : order.address.city,
            recipient_address: order.address.source,
            recipient_postalcode: order.address.postcode,
            recipient_email: order.email,
            recipient_phone: order.phone,
            recipient_address_note: '',
            delivery_terms: '4',
            description_contents: 'книга',
            order_seats_number: '1',
            order_weight: '0.5',
            letter_guarantee: 'нет',
            amount: '0',
            product_title: 'детская книга',
            product_articul: order_id,
            product_quantity: product_quantity,
            product_price: nil,
            product_cost: order.cash_on_delivery ? order.amount : '0',
            product_delivery_cost: '0'
          }
        end
      end

    end
  end
end
