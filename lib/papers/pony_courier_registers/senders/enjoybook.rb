module Papers
  module PonyCourierRegisters
    module Senders
      class Enjoybook

        def self.info
          {
            sender_country: 'Россия',
            sender_region: 'Оренбургская',
            sender_area: nil,
            sender_city: 'Оренбург',
            sender_address: 'Туркестанская 61',
            sender_postcode: Papers.configuration.postcode,
            sender_email: Papers.configuration.email,
            sender_phone: Papers.configuration.phone,
            sender_person: 'Белоусов В. В.',
            sender_note: nil
          }
        end

      end
    end
  end
end
