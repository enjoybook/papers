module Papers
  module PonyCourierRegisters
    module Senders
      class Mynamebook

        def self.info(name)
          {
            sender_country: 'Россия',
            sender_region: 'Москва',
            sender_area: nil,
            sender_city: 'Москва',
            sender_address: address(name),
            sender_postcode: Papers.configuration.postcode,
            sender_email: Papers.configuration.email,
            sender_phone: Papers.configuration.phone,
            sender_person: 'ИП Арестов А.А.',
            sender_note: nil
          }
        end

        private

        def self.address(name)
          case name
          when 'lettersdigits'
            'Дербеневская дом 24 стр 3'
          when 'netprint'
            'Котляковская ул, д 3 стр 13'
          else
            'пер. 2-й Кожевничевский дом 12'
          end
        end

      end
    end
  end
end
