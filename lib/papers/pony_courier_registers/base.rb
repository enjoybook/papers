require 'rubyXL'
require 'papers/helpers/file_helpers'

module Papers
  module PonyCourierRegisters
    class Base

      include Papers::FileHelpers

      COUNTRY = 'Россия'.freeze

      START_ROW = 2
      TEMPLATE = 'pony_courier.xlsx'.freeze

      def generate_file(orders, params = {}, basename = 'pony_courier')
        workbook = open_template_table
        data = prepare_data(orders, sender_info(params[:outsource] || params[:sender]))
        add_content(workbook, data)
        xlsx_to_temp(workbook, basename)
      end

      protected

      def prepare_data(orders, sender_info)
        raise NotImplementedError
      end

      def sender_info(sender_name)
        raise NotImplementedError
      end

      def add_content(workbook, rows_data)
        worksheet = workbook[0]

        rows_data.each_with_index do |row, row_index|
          row_index += self.class::START_ROW
          row.values.each_with_index do |value, cell_index|
            worksheet.add_cell row_index, cell_index, value
          end
        end
      end

      def open_template_table
        path = template_path self.class::TEMPLATE, true
        RubyXL::Parser.parse path
      end

      def international_sending?(order)
        order.address.country.mb_chars.downcase.to_s == COUNTRY.mb_chars.downcase.to_s
      end
    end
  end
end
