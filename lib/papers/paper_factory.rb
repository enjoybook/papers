module Papers
  class PaperFactory

    def self.build(namespace, new_version: false)
      project = Papers.configuration.project_name
      # temporary till migration to new partial post is over
      klass_name = new_version ? 'NewEnjoybook' : project.to_s.capitalize
      full_name = "Papers::#{namespace}::#{klass_name}"
      Object.const_get(full_name).new
    end

  end
end
