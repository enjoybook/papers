require 'papers/paper_factory'
require 'papers/qr_stickers/enjoybook'
require 'papers/qr_stickers/mynamebook'

module Papers
  class QrSticker

    class << self
      def new
        Papers::PaperFactory.build('QrStickers')
      end
    end

  end
end
