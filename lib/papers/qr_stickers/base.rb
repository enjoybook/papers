require 'papers/helpers/prawn_helpers'
require 'papers/helpers/file_helpers'
require 'papers/helpers/qr_helpers'
require 'prawn'

module Papers
  module QrStickers
    class Base

      extend Prawn::Measurements
      include Papers::FileHelpers
      include Papers::PrawnHelpers
      include Papers::QrHelpers
      include Prawn::Measurements

      WIDTH = mm2pt(100)
      HEIGHT = mm2pt(72)

      def generate_file(orders, basename = 'qr_stickers')
        doc = generate(orders)
        pdf_to_temp(doc, basename)
      end

      def generate_string(orders)
        generate(orders).render
      end

      private

      def generate(orders)
        doc = create_document

        orders.each_with_index do |order, index|
          doc.start_new_page unless index.zero?
          render_sticker doc, order, HEIGHT
        end

        doc
      end

      def create_document
        doc = Prawn::Document.new page_size: [WIDTH, HEIGHT], margin: 0
        set_font_family(doc, gothic_font_family)
        doc.font 'Gothic'
        doc
      end

      def render_sticker(doc, order, offset)
        render_qr doc, order, offset
        render_id doc, order, offset
        render_info doc, order, offset
      end

      def render_qr(doc, order, offset)
        qr = qr_code_image order.qr_url
        size = mm2pt(35)
        doc.image qr, at: [0, offset], fit: [size, size]
      end

      def render_id(doc, order, offset)
        text = "<b>#{order.id}#{shipping_mark(order)}</b>"
        render_vertical_text doc, text, offset + mm2pt(3), mm2pt(8), 16, :right
        render_vertical_text doc, text, offset + mm2pt(3), mm2pt(97), 16, :right
      end

      def render_vertical_text(doc, text, offset, x_offset, size = 12, align=:left)
        doc.text_box text, at: [x_offset, offset], rotate: -90, width: mm2pt(72), size: size,
          align: align, inline_format: true
      end

    end
  end
end