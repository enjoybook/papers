require 'papers/qr_stickers/base'

module Papers
  module QrStickers
    class Enjoybook < Base

      private

      def render_info(doc, order, offset)
        text = order.line_items.group_by(&:sku).map do |sku, group|
          "#{sku}: #{group.count}"
        end.join('<br>')

        render_vertical_text doc, text, offset - mm2pt(3), mm2pt(97)
      end

      def shipping_mark(order)
        order.delivery == 'post' ? 'П' : 'К'
      end

    end
  end
end
