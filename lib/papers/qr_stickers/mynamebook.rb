require 'papers/qr_stickers/base'

module Papers
  module QrStickers
    class Mynamebook < Base

      private

      def render_info(doc, order, offset)
        items = order.line_items + order.line_items.map(&:options).flatten
        text = items.group_by(&:sku).map do |sku, group|
          "#{sku}: #{group.count}"
        end.join('<br>')

        render_vertical_text doc, text, offset - mm2pt(3), mm2pt(97)
      end

      def shipping_mark(order)
        order.shipping == 'post' ? 'П' : 'К'
      end

    end
  end
end
