require 'papers/helpers/prawn_helpers'
require 'papers/helpers/file_helpers'
require 'papers/helpers/qr_helpers'
require 'prawn'

module Papers
  class Inventory

    extend Prawn::Measurements
    include Papers::FileHelpers
    include Papers::PrawnHelpers
    include Papers::QrHelpers
    include Prawn::Measurements

    WIDTH = mm2pt(210)
    HEIGHT = mm2pt(148)

    def initialize(order)
      @order = order
    end

    def generate_file(basename = 'inventory')
      pdf_to_temp(pdf_document, basename)
    end

    def generate_string
      pdf_document.render
    end

    private

    def pdf_document
      @doc = create_document
      render
      @doc
    end

    def create_document
      Prawn::Document.new page_size: [WIDTH, HEIGHT], margin: 0
    end

    def render
      render_id
      render_qr
      render_info
    end

    def render_id
      text = "#{@order.id}"
      render_text(text, mm2pt(10), mm2pt(10), 25)
    end

    def render_qr
      qr = qr_code_image(@order.qr_url)
      size = mm2pt(35)
      @doc.image qr, at: [mm2pt(5), HEIGHT - mm2pt(5)], fit: [size, size]
    end

    def render_info
      items = @order.line_items + @order.line_items.map(&:options).flatten
      text = items.group_by(&:sku).map do |sku, group|
        "#{sku}: #{group.count}"
      end.join('<br>')

      render_text(text, mm2pt(20), mm2pt(10))
    end

    def render_text(text, x, y, size=16)
      @doc.text_box text,
        at: [x, y],
        rotate: 90,
        size: size,
        inline_format: true
    end

  end
end
