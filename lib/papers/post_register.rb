require 'papers/helpers/axlsx_helpers'

module Papers
  class PostRegister

    include Papers::AxlsxHelpers

    WIDTHS = [3, 4, 11, 37, 19, 5, 5, 5, 5].freeze

    ITEM_TYPE = 'заказная бандероль'.freeze

    LIST_ITEM_TYPE = 'заказных почтовых отправлений'.freeze

    COLUMNS = [
        :index,
        :id,
        :type,
        :address,
        :username,
        :track,
        :weight,
        :tariff,
        :receipt_number
    ].freeze

    POST_TABLE_HEADER = [
      '№ п/п',
      'id',
      'вид почтового отправления',
      'куда',
      'кому',
      'трек-номер',
      'вес отправления',
      'тариф за пересылку',
      '№ квитанции'
    ].freeze

    def generate_file(orders, basename = 'post')
      data = prepare_data orders

      create_xlsx_file('register') do |sheet|
        add_title sheet
        add_header sheet
        add_content sheet, data
        add_bottom sheet, orders.size
        add_settings sheet
      end
    end

    protected

    def prepare_data(orders)
      orders.each_with_index.map do |order, index|
        order2row(order, index)
      end
    end

    def order2row(order, index)
      address_and_postcode = "#{order.address.postcode} #{order.address.source}"

      {
        index: index + 1,
        id: order.id,
        type: self.class::ITEM_TYPE,
        address: address_and_postcode,
        username: order.username,
        track: order.track,
        weight: '',
        tariff: '',
        receipt_number: '',
      }
    end

    def add_title(sheet)
      title_styles = build_styles sheet, 16, { horizontal: :center, vertical: :center }, true
      subtitle_styles = build_styles sheet, 12, { horizontal: :center, vertical: :center }

      project_styles = subtitle_styles

      sheet.merge_cells 'A1:H1'
      sheet.merge_cells 'A2:H2'
      sheet.merge_cells 'A3:H3'
      sheet.merge_cells 'A4:H4'

      title = "СПИСОК от #{DateTime.now.strftime '%e.%m.%y'}"
      subtitle1 = "#{self.class::LIST_ITEM_TYPE}, поданных в ________ ОПС __________ почтомата"
      subtitle2 = 'Отправитель __ИП Арестов А.А.____________________________________________'
      project = "Проект: #{Papers.configuration.project_name}.ru"

      sheet.add_row [title],     style: title_styles,    height: 30
      sheet.add_row [subtitle1], style: subtitle_styles, height: 25
      sheet.add_row [subtitle2], style: subtitle_styles, height: 25
      sheet.add_row [project],   style: project_styles,  height: 25
    end

    def add_header(sheet)
      sheet.add_row
      styles = build_border_styles(sheet, 10, { wrap_text: true })
      sheet.add_row self.class::POST_TABLE_HEADER, style: styles, height: 55
    end

    def add_content(sheet, data)
      data.each do |d|
        values = self.class::COLUMNS.map { |column| d[column] }
        style = build_border_styles(sheet, 10, { wrap_text: true })
        # height mynamebook 34
        sheet.add_row values, style: style, height: 45
      end
    end

    def add_bottom(sheet, size)
      sheet.add_row [ nil ]
      sheet.add_row ["Всего заказных писем 0 Заказных бандеролей #{size}"]

      sheet.add_row [ nil ]
      sheet.add_row ['Подпись_____________________/ Арестов А.А.']

      sheet.add_row [ nil ]
      sheet.add_row ['Подпись_____________________/___________________']

      size = sheet.rows.count
      sheet.merge_cells "A#{size-4}:H#{size-4}"
      sheet.merge_cells "A#{size-2}:H#{size-2}"
      sheet.merge_cells "A#{size}:H#{size}"
    end

    def add_settings(sheet)
      set_margins sheet
      sheet.column_widths *self.class::WIDTHS
    end

  end
end
