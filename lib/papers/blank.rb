require 'prawn'
require 'rmagick'
require 'papers/helpers/file_helpers'
require 'papers/helpers/prawn_helpers'

module Papers
  class Blank

    include Papers::FileHelpers
    include Papers::PrawnHelpers
    include Prawn::Measurements

    def generate_file(orders, basename = 'blanks')
      doc = generate(orders)
      pdf_to_temp(doc, basename)
    end

    def generate_string(orders)
      generate(orders).render
    end

    private

    def generate(orders)
      doc = create_document

      orders.each_with_index do |order, index|
        doc.start_new_page unless index.zero?
        blank doc, order
      end

      doc
    end

    def blank(doc, order)
      filled_blank = fill_blank blank_layout, order
      image = image_to_temp filled_blank, 'blank'
      filled_blank.destroy!
      bound doc, image, 0, 0, mm2pt(210), mm2pt(297)
    end

    def blank_layout
      blank_path = image_path('blank_moscow.jpg')
      Magick::ImageList.new blank_path
    end

    def fill_blank(blank, order)
      fill_top_order_id blank, order
      fill_postal_transfer blank, order
      fill_order_id blank, order
      fill_track blank, order
      fill_postuser blank, order
      fill_postaddress blank, order
      fill_postcode blank, order
    end

    def fill_top_order_id(blank, order)
      draw_text blank, mm2pt(90), 0, "Заказ: #{order.id}", 60
    end

    def fill_postal_transfer(blank, order)
      remittance_without_trifle, remittance_trifle = order.get_money_parts
      txt = order.transfer_money_format remittance_without_trifle, remittance_trifle
      left = 855
      top = 795
      draw_text blank, left, top, txt, 38

      left1 = 1285
      left2 = 1545
      top2 = 747
      draw_text blank, left1, top2, remittance_without_trifle, 40
      draw_text blank, left2, top2, remittance_trifle, 40

      blank
    end

    def fill_order_id(blank, order)
      top = 1169
      left = 830
      square_width = 45
      cell_left_margin = 25

      order.id.to_s.split('').each_with_index do |char, i|
        x = left + cell_left_margin + i * square_width
        draw_text blank, x, top, char, 38
      end

      blank
    end

    def fill_track(blank, order)
      top = 1225
      left = 830
      square_width = 45
      cell_left_margin = 25

      order.track.split('').each_with_index do |char, i|
        x = left + cell_left_margin + i * square_width
        x += 7 if char == '-'
        draw_text blank, x, top, char, 38
      end

      blank
    end

    def fill_postuser(blank, order)
      top = 1270
      left = 1020
      draw_text blank, left, top, order.username, 30

      blank
    end

    def fill_postaddress(blank, order)
      top = 1340
      left = 1220
      draw_text blank, left, top, order.address.source
      blank
    end

    def fill_postcode(blank, order)
      top = 1400
      left = 2008
      square_width = 45
      cell_left_margin = 20

      order.address.postcode.split('').each_with_index do |char, i|
        x = left + (square_width * i) + cell_left_margin
        draw_text blank, x, top, char, 40
      end

      blank
    end

    def draw_text(image, left, top, text, sz=30)
      txt = Magick::Draw.new
      txt.font = font_path('HelveticaRegular.ttf')
      image.annotate(txt, 0, 0, left, top, text.to_s) do
        txt.pointsize = sz
        txt.fill = '#000000'
        txt.font_weight = 500
        txt.font_style = Magick::NormalStyle
        txt.gravity = Magick::NorthWestGravity
      end
      image
    end

  end
end
