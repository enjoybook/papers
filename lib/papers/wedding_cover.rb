require 'prawn'
require 'papers/helpers/prawn_helpers'
require 'papers/helpers/file_helpers'

module Papers
  class WeddingCover

    include Papers::FileHelpers
    include Papers::PrawnHelpers

    def generate_file(order, basename = 'wedding_cover')
      doc = generate(order)
      pdf_to_temp(doc, basename)
    end

    def generate_string(order)
      generate(order).render
    end

    private

    def generate(order)
      page_size = [mm2pt(291), mm2pt(1137)]
      doc = Prawn::Document.new(page_size: page_size, margin: 0, page_layout: :landscape)

      mockup_path = image_path('wedding_cover.jpg', true)
      set_font(doc, 'intro-light-alt.ttf')
      doc.stroke_color('aaaaaa')
      doc.line_width = 0.05

      add_wedding_cover doc, mockup_path, order

      doc
    end

    def add_wedding_cover(doc, mockup_path, order)
      doc.text_rendering_mode(:fill_stroke) do
        doc.image mockup_path, height: mm2pt(291), width: mm2pt(1137)

        doc.fill_color "03060c"
        doc.text_box "#{order.wedding_names}",
          at: [mm2pt(627), mm2pt(135)], width: mm2pt(180), align: :center, size: mm2pt(10)

        doc.fill_color "676663"
        doc.text_box "#{order.wedding_date}",
          at: [mm2pt(679), mm2pt(115)], width: mm2pt(80), align: :center, size: mm2pt(8)

        doc.fill_color "000000"
        doc.text_box "#{order.id}",
          at: [mm2pt(1), mm2pt(115)], width: mm2pt(80), align: :center, size: mm2pt(4), rotate: 90
      end
    end
  end
end
