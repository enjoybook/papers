module Papers
  class PostCashOnDeliveryRegister < PostRegister

    WIDTHS = [3, 5.3, 11, 37, 19, 5, 5, 5, 5, 7.6].freeze

    ITEM_TYPE = 'Бандероль с объявленной ценностью'.freeze

    LIST_ITEM_TYPE = 'Бандеролей с объявленной ценностью'.freeze

    POST_TABLE_HEADER = [
      '№ п/п',
      'id',
      'вид почтового отправления',
      'куда',
      'кому',
      'трек-номер',
      'вес отправления',
      'тариф за пересылку',
      '№ квитанции',
      'объявленная ценность'
    ].freeze

    COLUMNS = [
      :index,
      :id,
      :type,
      :address,
      :username,
      :track,
      :weight,
      :tariff,
      :receipt_number,
      :amount
    ].freeze

    protected

    def order2row(order, index)
      row = super(order, index)
      row[:amount] = post_amount(order)
      row
    end

    def post_amount(order)
      money, money_trifle = order.calc_post_remittance_amount
      "#{money}.#{money_trifle}"
    end

  end
end
