require 'papers/version'
require 'papers/configuration'

require 'papers/blank'
require 'papers/invoice'
require 'papers/wedding_cover'
require 'papers/magnet'
require 'papers/partial_post'
require 'papers/certificate'
require 'papers/sticker'
require 'papers/qr_sticker'
require 'papers/inventory'

require 'papers/courier_register'
require 'papers/post_register'
require 'papers/post_cash_on_delivery_register'
require 'papers/pony_courier_register'
require 'papers/pony_outsource_register'
require 'papers/game_register'

module Papers

  def self.root
    File.expand_path '../..', __FILE__
  end

end
