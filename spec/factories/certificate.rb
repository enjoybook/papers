require 'ostruct'
require 'dummy/models/certificate'

FactoryBot.define do
  factory :certificate do
    sequence(:id)
    mockup 'mockup1'
    pages_count 1
    promo { OpenStruct.new(code: '5341169') }
    promos { [promo] }

    skip_create
  end
end
