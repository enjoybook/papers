require 'dummy/models/address'

FactoryBot.define do
  factory :address do
    sequence(:id)
    source 'Москва, ул Гримау 27/4, кв 46'
    postcode '560040'
    country 'Россия'
    region 'Москва'
    area ''
    city 'Москва'
    settlement ''
    street 'Гримау'
    house '27/4'
    flat '46'
    status 0

    skip_create

    after(:build) do |order|
      allow(order).to receive(:region_full).and_return('Московская область')
      allow(order).to receive(:area_full).and_return('город')
      allow(order).to receive(:locality_full).and_return('Москва')
      allow(order).to receive(:street_full).and_return('Гримау 27/4')
    end
  end
end
