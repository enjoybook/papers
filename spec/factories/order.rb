require 'ostruct'
require 'dummy/models/order'

FactoryBot.define do
  factory :order do
    sequence(:id)
    username 'Ленар Ахмадеев'
    email 'lenarakhmadeev@gmail.com'
    phone '89033698787'
    paid_at '21.12.2017'
    track '46006004000802'
    weight '2.46'
    shipping 'post'
    delivery 'post'
    cash_on_delivery true
    amount 2400
    cards_count 4
    wedding_names 'Иван и Мария'
    wedding_date { DateTime.now.next_month.to_date }
    created_at { DateTime.now }
    sent_at { DateTime.now }
    product_name { 'Фотокнига' }
    books { [build(:book), build(:book)] }
    total_books 2
    games []
    voicebooks []
    line_items_count 2
    total 25
    address

    after(:build) do |order|
      allow(order).to receive(:get_money_parts).and_return([2400, 00])
      allow(order).to receive(:simple_money_format).and_return('2400 руб., 0 коп.')
      allow(order).to receive(:calc_post_remittance_amount).and_return([320, 0])
      transfer_money_format = 'две тысячи четыреста рублей, 0 копеек'
      allow(order).to receive(:transfer_money_format).and_return(transfer_money_format)
      allow(order).to receive(:predict_sending_time).and_return(DateTime.now.next_month)
      allow(order).to receive(:qr_url).and_return('https://enjoybook.ru/')
      allow(order).to receive(:is_post?).and_return(order.shipping == 'post')
      allow(order).to receive(:parcel?).and_return(false)
      allow(order).to receive(:heavy_parcel?).and_return(false)
      allow(order).to receive(:cash_on_delivery?).and_return(order.cash_on_delivery)
      allow(order).to receive(:shipping_method_name).and_return('Почта бандероль (наложка)')
    end
  end
end
