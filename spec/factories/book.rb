require 'dummy/models/book'

FactoryBot.define do
  factory :book do
    sequence(:id)
    sku :book
    pages_count 25
    options { [OpenStruct.new(sku: 'PMNB'), OpenStruct.new(sku: 'PFB')] }
  end
end
