RSpec.shared_context 'enjoybook', project: :enjoybook do
  before do
    Papers.configure { |c| c.project_name = :enjoybook }
  end

  after do
    Papers.configure { |c| c.project_name = :mynamebook }
  end
end
