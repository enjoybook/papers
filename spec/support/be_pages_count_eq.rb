require 'pdf/inspector'

RSpec::Matchers.define :be_pages_count_eq do |expected|
  match do |actual|
    page_analysis = PDF::Inspector::Page.analyze actual
    page_analysis.pages.size == expected
  end

  failure_message do |actual|
    "expected PDF pages count #{get_pages_count(actual)} to be equal #{expected}"
  end

  failure_message_when_negated do |actual|
    "expected PDF pages count #{get_pages_count(actual)} NOT to be equal #{expected}"
  end

  def get_pages_count(pdf_string)
    page_analysis = PDF::Inspector::Page.analyze pdf_string
    page_analysis.pages.size
  end
end