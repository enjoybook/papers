require 'bundler/setup'
require 'papers'
require 'factory_bot'
require 'byebug'

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
  config.example_status_persistence_file_path = ".rspec_status"
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    FactoryBot.find_definitions
    Papers.configure do |c|
      c.project_name = :mynamebook
      c.phone = '8 (800) 555-11-90'
      c.postcode = '460000'
      c.email = 'enjoybook.ru@gmail.com'
    end
  end
end

FactoryBot::SyntaxRunner.class_eval do
  include RSpec::Mocks::ExampleMethods
end

Dir[File.expand_path(File.join(File.dirname(__FILE__),'support','**','*.rb'))].each {|f| require f}
