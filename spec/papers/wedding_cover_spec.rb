require 'spec_helper'

RSpec.describe Papers::WeddingCover do
  include_context 'enjoybook'

  let(:order) { build(:order) }
  subject { Papers::WeddingCover.new }

  describe '#generate_file' do
    it 'returns file' do
      result = subject.generate_file(order)
      FileUtils.mv(result.path, 'spec/output/wedding_cover.pdf')

      expect(result).to be_a(Tempfile)
    end
  end

  describe '#generate_string' do
    it 'returns wedding cover string' do
      result = subject.generate_string(order)

      expect(result).to be_pages_count_eq(1)
    end
  end

end
