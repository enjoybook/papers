require 'spec_helper'

RSpec.describe Papers::Stickers::Enjoybook do
  include_context 'enjoybook'

  let(:orders) { build_list(:order, 2) }
  subject { Papers::Stickers::Enjoybook.new }

  describe '#generate_file' do
    context 'default' do
      it 'returns file' do
        result = subject.generate_file(orders, format: :default)
        FileUtils.mv(result.path, 'spec/output/stickers_enjoybook.pdf')

        expect(result).to be_a(Tempfile)
      end
    end

    context 'a4' do
      it 'returns file' do
        result = subject.generate_file(orders, format: :a4)
        FileUtils.mv(result.path, 'spec/output/stickers_enjoybook_a4.pdf')

        expect(result).to be_a(Tempfile)
      end
    end
  end

  describe '#generate_string' do
    context 'default' do
      it 'returns stickers string' do
        result = subject.generate_string(orders)

        expect(result).to be_pages_count_eq(2)
      end
    end

    context 'a4' do
      it 'returns stickers string' do
        result = subject.generate_string(orders, format: :a4)

        expect(result).to be_pages_count_eq(1)
      end
    end
  end

end
