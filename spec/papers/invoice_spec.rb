require 'spec_helper'

RSpec.describe Papers::Invoice do
  let(:order) { build(:order) }
  let(:address) { order.address }
  subject { Papers::Invoice.new(invoices_count: 6) }
  before do
    allow(order).to receive(:line_items).and_return([
      {
        name_en: 'Photo’s book, hard cover, cardboard cover, pages, paper, 20 pages  22*22 sm, publishing enjoybook.ru, 2018',
        name: 'Фото книга, твёрдый переплет, страницы бумага, 20 страниц 22x22 сантиметра, издательство enjoybook.ru, 2018 год',
        price: 10,
        quantity: 1,
        index: 1
      },
      {
        name_en: 'Photo’s book, hard cover, cardboard cover, pages, paper, 40 pages  22*22 sm, publishing enjoybook.ru, 2018',
        name: 'Фото книга, твёрдый переплет, страницы бумага, 40 страниц 22x22 сантиметра, издательство enjoybook.ru, 2018 год',
        price: 20,
        quantity: 4,
        index: 2
      }
    ])
    allow(order).to receive(:description_items_by_type).and_return(
      { count_items_by_type: {
          'Детская персональная книга в количестве' => 1, 'Семейная книга в количестве' => 1
        }
      }
    )
  end

  describe '#generate_file' do
    context 'mynamebook' do
      let(:file) { File.open 'spec/output/mynamebook_invoice.docx', 'w' }

      it 'returns file' do
        allow(Tempfile).to receive(:new).and_return(file)
        result = subject.generate_file([order, order, order, order, order])

        expect(result).to be_a(File)
      end
    end

    context 'enjoybook' do
      let(:file) { File.open 'spec/output/enjoybook_invoice.docx', 'w' }
      before { Papers.configuration.project_name = :enjoybook }
      after { Papers.configuration.project_name = :mynamebook }

      it 'returns file' do
        allow(Tempfile).to receive(:new).and_return(file)
        result = subject.generate_file([order, order, order, order, order])

        expect(result).to be_a(File)
      end
    end
  end

  describe '#generate_string' do
    it 'returns string' do
      result = subject.generate_string([order])

      expect(result).to be_a(String)
    end
  end

  describe '#template_params' do
    before do
      allow(order.books).to receive(:count).and_return(2)
    end

    it 'returns invoice template params' do
      result = subject.send(:template_params, [order]).first

      expect(result[:invoices][0]).to include(
        username: str_with_translit(order.username),
        address: str_with_translit([address.street, address.house, address.flat].join(', ')),
        city_and_postcode: str_with_translit([address.city, address.postcode].compact.join(', ')),
        country: str_with_translit(address.country),
        line_items_count: order.line_items_count,
        phone: order.phone
      )
    end

    it 'returns guarantee template params' do
      result = subject.send(:template_params, [order]).first

      expect(result[:guarantees][0]).to include(
        date: order.predict_sending_time.strftime('%d.%m.%Y'),
        country: address.country,
        products: [' Детская персональная книга в количестве в количестве 1 шт.',
                   ' Семейная книга в количестве в количестве 1 шт.']
      )
    end

    def str_with_translit(str)
      [str, ::Russian.translit(str)].join(' / ')
    end
  end
end
