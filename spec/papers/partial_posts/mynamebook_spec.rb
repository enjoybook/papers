require 'spec_helper'
require 'axlsx'
require 'rubyXL'

RSpec.describe Papers::PartialPosts::Mynamebook do
  let(:order) { build(:order) }
  subject { Papers::PartialPosts::Mynamebook.new }

  describe '#generate_file' do
    it 'returns file' do
      result = subject.generate_file([order])
      FileUtils.mv(result.path, 'spec/output/mynamebook_partial_post.csv')

      expect(result).to be_a(Tempfile)
    end
  end

  describe '#generate_string' do
    it 'returns enjoybook partial post string' do
      result = subject.generate_string([order])
      result = CSV.parse(result, col_sep: ';')
      row = result[1]

      expect(row[0]).to eq(order.track)
      expect(row[1]).to eq(order.address.postcode)
      expect(row[5]).to eq(order.address.region_full.encode('cp1251'))
    end
  end

end
