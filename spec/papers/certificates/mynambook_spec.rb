require 'spec_helper'

RSpec.describe Papers::Certificates::Mynamebook do
  let(:certificate) { build(:certificate) }
  subject { Papers::Certificates::Mynamebook.new }

  describe '#generate_file' do
    it 'returns file' do
      result = subject.generate_file(certificate)
      FileUtils.mv(result.path, 'spec/output/mynamebook_certificate.pdf')

      expect(result).to be_a(Tempfile)
    end
  end

  describe '#generate_string' do
    it 'returns string' do
      result = subject.generate_string(certificate)

      expect(result).to be_a(String)
    end

    it 'returns certificate' do
      result = subject.generate_string(certificate)

      expect(result).to be_pages_count_eq(1)
    end
  end
end
