require 'spec_helper'

RSpec.describe Papers::Certificates::Enjoybook do
  include_context 'enjoybook'

  let(:certificate) { build(:certificate) }
  subject { Papers::Certificates::Enjoybook.new }

  describe '#generate_file' do
    it 'returns file' do
      result = subject.generate_file(certificate)
      FileUtils.mv(result.path, 'spec/output/enjoybook_certificate.pdf')

      expect(result).to be_a(Tempfile)
    end
  end

  describe '#generate_string' do
    it 'returns certificate string' do
      result = subject.generate_string(certificate)

      expect(result).to be_pages_count_eq(1)
    end
  end

end
