require 'spec_helper'
require 'axlsx'
require 'rubyXL'

RSpec.describe Papers::CourierRegister do
  let(:games) { [OpenStruct.new(id: 1)] }
  let(:orders) { build_list(:order, 2, games: games) }
  let(:file) { File.open 'spec/output/games.xlsx', 'w' }
  let(:axlsx_package) { Axlsx::Package.new }

  subject { Papers::GameRegister.new }

  before do
    axlsx_package.use_shared_strings = true
    allow(Axlsx::Package).to receive(:new).and_return(axlsx_package)
    allow(Tempfile).to receive(:new).and_return(file)
  end

  describe '#generate_file' do
    let(:index) { '1' }

    it 'returns game register file' do
      result = subject.generate_file(orders)

      workbook = RubyXL::Parser.parse(result.path)
      worksheet = workbook.worksheets[0]

      row_values = worksheet.sheet_data[6].cells.map(&:value)
      order = orders[0]

      expect(row_values[0]).to eq(index)
      expect(row_values[1]).to eq(order.id)
      expect(row_values[2]).to eq(order.games.size)
      expect(row_values[3]).to eq(order.username)
      expect(row_values[4]).to eq(order.shipping)
    end
  end

end
