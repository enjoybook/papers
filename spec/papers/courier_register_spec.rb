require 'spec_helper'
require 'axlsx'
require 'rubyXL'

RSpec.describe Papers::CourierRegister do
  let(:orders) { build_list(:order, 2) }
  let(:file) { File.open 'spec/output/registers_courier.xlsx', 'w' }
  let(:axlsx_package) { Axlsx::Package.new }
  subject { Papers::CourierRegister.new }

  before do
    axlsx_package.use_shared_strings = true
    allow(Axlsx::Package).to receive(:new).and_return(axlsx_package)
    allow(Tempfile).to receive(:new).and_return(file)
  end

  describe '#generate_file' do
    it 'returns courier register file' do
      result = subject.generate_file(orders)

      workbook = RubyXL::Parser.parse(result.path)
      worksheet = workbook.worksheets[0]
      row_values = worksheet.sheet_data[10].cells.map { |c| c.value }
      order = orders[0]

      expect(row_values[0]).to eq(2)
      expect(row_values[1]).to eq(1)
      expect(row_values[2]).to eq(order.product_name)
      expect(row_values[3]).to eq(order.address.source)
      expect(row_values[8]).to eq(order.username)
      expect(row_values[9].to_s).to eq(order.phone)
    end
  end

end
