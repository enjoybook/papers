require 'spec_helper'

RSpec.describe Papers::QrSticker do
  let(:order) { build(:order) }
  subject { Papers::Inventory.new(order) }

  before do
    allow(order).to receive(:line_items).and_return(order.books)
  end

  describe '#generate_file' do
    it 'returns file' do
      result = subject.generate_file
      FileUtils.mv(result.path, 'spec/output/inventory.pdf')

      expect(result).to be_a(Tempfile)
    end
  end

end
