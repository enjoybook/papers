require 'ostruct'
require 'spec_helper'

RSpec.describe Papers::Magnet do
  include_context 'enjoybook'

  let(:mosaic_file) { File.open('spec/fixtures/test.png') }
  let(:book) { build(:book) }
  let(:order) { build(:order, books: [book]) }
  subject { Papers::Magnet.new }

  before do
    magnets_mosaic = OpenStruct.new(url: '')
    allow(book).to receive(:magnets_mosaic).and_return(magnets_mosaic)
    allow(subject).to receive(:open).and_return(mosaic_file)
  end

  after { mosaic_file.close }

  describe '#generate_file' do
    it 'returns file' do
      result = subject.generate_file([order])
      FileUtils.mv(result.path, 'spec/output/magnet.pdf')

      expect(result).to be_a(Tempfile)
    end
  end

  describe '#generate_string' do
    it 'returns magnet string' do
      result = subject.generate_string([order])

      expect(result).to be_pages_count_eq(1)
    end
  end

end
