require 'spec_helper'
require 'axlsx'
require 'rubyXL'

RSpec.describe Papers::PonyCourierRegisters::Enjoybook do
  let(:order) { build(:order) }
  let(:file) { File.open 'spec/output/enjoybook_pony_register.xlsx', 'w' }

  subject { Papers::PonyCourierRegisters::Enjoybook.new }

  before do
    allow(Tempfile).to receive(:new).and_return(file)
  end

  describe '#generate_file' do
    it 'returns enjoybook pony courier register file' do
      result = subject.generate_file([order])
      row_values = read_row_values(result.path, 2)

      expect(row_values[0]).to eq('6-775')
      expect(row_values[2]).to eq(order.email)
      expect(row_values[3]).to eq('Enjoybook.ru')
      expect(row_values[8]).to eq('Туркестанская 61')
    end

    it 'sets contract code for Russia' do
      russia_contract_code = '6-775'

      result = subject.generate_file([order])
      row_values = read_row_values(result.path, 2)

      expect(row_values[0]).to eq(russia_contract_code)
    end

    it 'sets contract code for foreign countries' do
      international_contract_code = '6-798'
      order.address.country = 'Германия'

      result = subject.generate_file([order])
      row_values = read_row_values(result.path, 2)

      expect(row_values[0]).to eq(international_contract_code)
    end
  end

  def read_row_values(path, row)
    workbook = RubyXL::Parser.parse(path)
    worksheet = workbook.worksheets[0]
    worksheet.sheet_data[row].cells.map { |c| c.value }
  end
end
