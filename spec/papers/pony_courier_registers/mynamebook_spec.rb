require 'spec_helper'
require 'axlsx'
require 'rubyXL'

RSpec.describe Papers::PonyCourierRegisters::Mynamebook do
  let(:order) { build(:order) }
  let(:file) { File.open 'spec/output/mynamebook_pony_register.xlsx', 'w' }
  subject { Papers::PonyCourierRegisters::Mynamebook.new }

  before do
    allow(Tempfile).to receive(:new).and_return(file)
  end

  describe '#generate_file' do
    it 'returns mynamebook pony courier register file' do
      result = subject.generate_file([order])
      row_values = read_row_values(result.path, 2)

      expect(row_values[0]).to eq('6-776')
      expect(row_values[2]).to eq(order.email)
      expect(row_values[3]).to eq('Mynamebook.ru')
      expect(row_values[8]).to eq('пер. 2-й Кожевничевский дом 12')
    end

    context 'with lettersdiggits outsource' do
      it 'contains lettersdiggits address' do
        result = subject.generate_file([order], { outsource: 'lettersdigits' })
        row_values = read_row_values(result.path, 2)

        expect(row_values[8]).to eq('Дербеневская дом 24 стр 3')
      end
    end

    it 'sets contract code for Russia' do
      russia_contract_code = '6-776'

      result = subject.generate_file([order])
      row_values = read_row_values(result.path, 2)

      expect(row_values[0]).to eq(russia_contract_code)
    end

    it 'sets contract code for foreign countries' do
      international_contract_code = '6-799'
      order.address.country = 'Германия'

      result = subject.generate_file([order])
      row_values = read_row_values(result.path, 2)

      expect(row_values[0]).to eq(international_contract_code)
    end
  end

  def read_row_values(path, row)
    workbook = RubyXL::Parser.parse(path)
    worksheet = workbook.worksheets[0]
    worksheet.sheet_data[row].cells.map { |c| c.value }
  end
end
