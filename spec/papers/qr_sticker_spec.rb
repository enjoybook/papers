require 'spec_helper'

RSpec.describe Papers::QrSticker do
  let(:orders) { build_list(:order, 2) }
  subject { Papers::QrSticker.new }

  before do
    allow(orders[0]).to receive(:line_items).and_return(orders[0].books)
    allow(orders[1]).to receive(:line_items).and_return(orders[1].books)
  end

  describe '#generate_file' do
    it 'returns file' do
      result = subject.generate_file(orders)
      FileUtils.mv(result.path, 'spec/output/stickers_qr.pdf')

      expect(result).to be_a(Tempfile)
    end
  end

  describe '#generate_string' do
    it 'returns qr stickers string' do
      result = subject.generate_string(orders)

      expect(result).to be_pages_count_eq(2)
    end
  end

end
