require 'spec_helper'
require 'axlsx'
require 'rubyXL'

RSpec.describe Papers::PonyOutsourceRegisters::Mynamebook do
  let(:order) { build(:order) }
  let(:file) { File.open 'spec/output/mynamebook_pony_outsource_register.xlsx', 'w' }
  subject { Papers::PonyOutsourceRegisters::Mynamebook.new }

  before do
    allow(Tempfile).to receive(:new).and_return(file)
  end

  describe '#generate_file' do
    it 'returns mynamebook pony outsource register file' do
      result = subject.generate_file([order])
      row_values = read_row_values(result.path, 4)

      expect(row_values[1]).to eq(order.track)
      expect(row_values[4]).to eq('пер. 2-й Кожевничевский дом 12')
      expect(row_values[9]).to eq(order.address.source)
    end
  end

  def read_row_values(path, row)
    workbook = RubyXL::Parser.parse(path)
    worksheet = workbook.worksheets[0]
    worksheet.sheet_data[row].cells.map { |c| c.value }
  end
end
