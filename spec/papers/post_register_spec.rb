require 'spec_helper'
require 'axlsx'
require 'rubyXL'

RSpec.describe Papers::PostRegister do
  let(:order) { build(:order) }
  let(:file) { File.open 'spec/output/post_register.xlsx', 'w' }
  let(:axlsx_package) { Axlsx::Package.new }
  subject { Papers::PostRegister.new }

  before do
    axlsx_package.use_shared_strings = true
    allow(Axlsx::Package).to receive(:new).and_return(axlsx_package)
    allow(Tempfile).to receive(:new).and_return(file)
  end

  describe '#generate_file' do
    it 'returns post register file' do
      result = subject.generate_file([order])

      workbook = RubyXL::Parser.parse(result.path)
      worksheet = workbook.worksheets[0]
      row_values = worksheet.sheet_data[6].cells.map { |c| c.value }

      expect(row_values[0]).to eq(1)
      expect(row_values[1]).to eq(order.id)
      expect(row_values[2]).to eq('заказная бандероль')
      expect(row_values[3]).to eq("#{order.address.postcode} #{order.address.source}")
      expect(row_values[4]).to eq(order.username)
    end
  end

end
