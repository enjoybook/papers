require 'spec_helper'

RSpec.describe Papers::Blank do
  let(:orders) { build_list(:order, 2) }
  subject { Papers::Blank.new }

  describe '#generate_file' do
    it 'returns file' do
      result = subject.generate_file(orders)
      FileUtils.mv(result.path, 'spec/output/blank.pdf')

      expect(result).to be_a(Tempfile)
    end
  end

  describe '#generate_string' do
    it 'returns blakns string' do
      result = subject.generate_string(orders)

      expect(result).to be_pages_count_eq(2)
    end
  end

end
