class Order
  attr_accessor :id, :username, :email, :phone, :paid_at, :track, :weight, :cash_on_delivery,
                :amount, :address, :books, :shipping, :games, :voicebooks, :delivery,
                :wedding_names, :wedding_date, :created_at, :sent_at, :product_name,
                :shipping_method_name, :cards_count, :line_items, :line_items_count, :total,
                :total_books
end
